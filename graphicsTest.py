

import graphics2D
from graphics2D import UDim2
from vector import Vector2

graphics2D.setResolution()
graphics2D.setTitle("Test Graphics 2D")


guiLayer = graphics2D.layer_newGuiLayer()
testLayer = graphics2D.layer_newVectorielLayer()

surface_terrain = graphics2D.Gui_Frame(UDim2(0,0,0,0),UDim2(0,200,0,200),(130,211,34,255),None,2)
# surface_terrain.surface_render()
surface_test1 = graphics2D.Gui_Frame(UDim2(0,0,0,0),UDim2(0,30,0,30),(130,211,100,255),None,2)
# surface_test1.surface_render()
surface_test2 = graphics2D.Gui_Frame(UDim2(0,10,0,10),UDim2(0,30,0,30),(130,211,150,255),None,2)
# surface_test2.surface_render()
surface_cards = graphics2D.Gui_Frame(UDim2(0,10,0,220),UDim2(0,150,0,50),(0,0,0,255),None,2)
# surface_cards.surface_render()

# surface_drawable = graphics2D.Gui_Frame_Drawable(UDim2(0,20,0,20),UDim2(0,200,0,200),(50,50,50,255),None,4)
# guiLayer.objects_add(surface_drawable)
# surface_drawable.objects_add(graphics2D.VecCercle(Vector2(100,100),20,(0,0,0,255),None,0))

frameB = graphics2D.Gui_Frame_Scrollable(UDim2(0,20,0,20),UDim2(0,360,0,360),(0,0,0,255),None,1)

frameA = graphics2D.Gui_Frame(UDim2(0,10,0,10),UDim2(0,400,0,400),(0,0,0,255),None,1)
guiLayer.objects_add(frameA)
frameA.objects_add(frameB)

frameB.objects_add(surface_terrain)
frameB.objects_add(surface_cards)
frameB.objects_add(surface_test1)
frameB.objects_add(surface_test2)

testLayer.objects_add(graphics2D.VecCercle(Vector2(20,20),20,(0,0,0,255),None,5))


guiLayer.surface_render_all()


speedCamera = 2
def camera_pos_set(pos):
    frameB.cameraPos = pos
def camera_pos_add(delta):
    frameB.cameraPos += delta*speedCamera*frameB.zoom_get()
    # frameC1.pos += delta*speedCamera
def camera_zoom_mouseScroll(scrollY):
    mousePos = graphics2D.getMousePosition()
    mousePos += frameB.pos.vector2()

    zoomLast = frameB.zoom_get()
    cameraPos = frameB.cameraPos
    cameraZoom = zoomLast
    if scrollY>0:
        cameraZoom *= 2
    elif scrollY<0:
        cameraZoom //= 2
    frameB.zoom_set(cameraZoom)

    if zoomLast!=cameraZoom:
        if scrollY>0:
            camera_pos_set(cameraPos - mousePos/cameraZoom )
        else:
            camera_pos_set(cameraPos + mousePos/zoomLast )



    frameB.surface_render()
    frameA.surface_render()
    guiLayer.surface_render()
    graphics2D.render()
def render_terrain_move():
    frameB.surface_render()
    frameA.surface_render()
    guiLayer.surface_render()
    graphics2D.render()


# ----- # Mouvements # ----- # 
def camera_move_left():
    camera_pos_add(Vector2(0-1,0))
    render_terrain_move()
def camera_move_right():
    camera_pos_add(Vector2(1,0))
    render_terrain_move()
def camera_move_top():
    camera_pos_add(Vector2(0,0-1))
    render_terrain_move()
def camera_move_down():
    camera_pos_add(Vector2(0,1))
    render_terrain_move()
graphics2D.addLinkInput_twoState("K_z",camera_move_top)
graphics2D.addLinkInput_twoState("K_s",camera_move_down)
graphics2D.addLinkInput_twoState("K_d",camera_move_right)
graphics2D.addLinkInput_twoState("K_q",camera_move_left)

def mouseScroll(scrollY):
    camera_zoom_mouseScroll(scrollY)
    render_terrain_move()
graphics2D.addLink_MouseScroll(mouseScroll)


# ----- # Render # ----- # 
graphics2D.render()

while graphics2D.ifNotQuit():
    graphics2D.wait(20)









