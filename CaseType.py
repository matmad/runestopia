
class CaseType:
    grass = 1
    water = 2
    tree  = 3
    path  = 4
    wall  = 5


class Class_CaseInfo:
    def __init__(self, id, name, density,level):
        self.id:int = id
        self.name:str = name

        self.level:int = level
        self.density:int = density


class Class_CasesInfo:
    dicTypeId = {}
    dicTypeName = {}

    def __getitem__(self, key):
        if isinstance(key,int):
            return self.dicTypeId.__getitem__(key)
        elif isinstance(key,str):
            return self.dicTypeName.__getitem__(key)
        assert False, "Key type not Supported"
        

    def __setitem__(self, key, v):
        self.dicType.__setitem__(key, v)

    def __len__(self):
        return len(self.dicType)
    
    def new(self, id, name, density,level):
        newCaseInfo = Class_CaseInfo(id, name, density,level)
        self.dicTypeId[id] = newCaseInfo
        self.dicTypeName[name] = newCaseInfo


InfoCases = Class_CasesInfo()
InfoCases.new(0, "none"  , -1 , 0)
InfoCases.new(1, "grass" , 2  , 0)
InfoCases.new(2, "water" , 7  , 0)
InfoCases.new(3, "tree"  , 3  , 0)
InfoCases.new(4, "path"  , 1  , 0)
InfoCases.new(5, "wall"  , 10 , 1)


print(InfoCases[1].name)
print(InfoCases["grass"].name)

# class CaseType:
#     grass = 1
#     water = 2
#     tree  = 3
#     path  = 5
#     wall  = 4