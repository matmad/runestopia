

from entity_sprites import EntityTiles, new_EntityTiles
from graphics2D import Vector2
import json


class EntityType:
    soldat = 1
    paladin = 2
    witch = 3
    zombie = 4
    skelton = 5





class ReferenceEntity:
    def __init__(self,data:dict) -> None:
        self.load_EntityTiles(data)

        self.maxHealth = data["health"]
        self.id = -1

    def load_EntityTiles(self,data):
        startPos = Vector2(data["tiles"]["start"][0],data["tiles"]["start"][1])
        endPos = Vector2(data["tiles"]["end"][0],data["tiles"]["end"][1])
        
        listOfTilesSelected = data["animation"]["front"],data["animation"]["back"],data["animation"]["left"],data["animation"]["right"]
        
        self.tiles:EntityTiles = new_EntityTiles(startPos,endPos,listOfTilesSelected)
    
    def getSprites(self,velocity,i):
        if velocity.x == 0 and velocity.y == 0:
            # Immobile / don't move 
            return self.tiles[0]
        elif velocity.x > 0:
            return self.tiles.get_left(i)
        elif velocity.x < 0:
            return self.tiles.get_right(i)
        elif velocity.y > 0:
            return self.tiles.get_front(i)
        elif velocity.y < 0:
            return self.tiles.get_back(i)
        

        
class Entity:
    def __init__(self,reference,pos:Vector2):
        self.ID = 0
        self.r:ReferenceEntity = reference # reference 
        self.tId = self.r.id
        
        self.pos:Vector2 = pos
        self.health = 10





entityList = list()


# --- # Open with Json File # --- # 
# Opening JSON file
f = open('ressources\entity.json')

# returns JSON object as 
data = json.load(f)


# Iterating through the json
# list
for i in data:
    print(" > ",i['name'])
    
    newEntity = ReferenceEntity(i)
    entityList.append(newEntity)

# Closing file
f.close()
    


