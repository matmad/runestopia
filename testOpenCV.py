
import cv2
import matplotlib.pyplot as plt
from vector import Vector2

template = cv2.imread("ressources/templates/template_foret.png",-1) # BGR

imageShape = template.shape
sizeImage = Vector2(imageShape[1],imageShape[0])
sizeTile = Vector2(16,16)

for ix in range(sizeImage.x//sizeTile.x):
    template = cv2.line(template, (ix*sizeTile.x,0),(ix*sizeTile.x,sizeImage.y),(255,0,0),1)

for iy in range(sizeImage.y//sizeTile.y):
    template = cv2.line(template, (0,iy*sizeTile.y),(sizeImage.x,iy*sizeTile.y),(255,0,0),1)

def getRegion(start:Vector2,end:Vector2):
    return template[start.x:end.x, start.y:end.y]


# plt.imshow(getRegion(Vector2(0,0),sizeTile)[:,:,::-1])
# plt.imshow(template[:,:,::-1])
plt.imshow(template[:,:,2::-1])
plt.show()