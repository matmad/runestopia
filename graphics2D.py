

# ----- # Import : Pygame # ----- #
import pygame
from pygame.locals import K_RETURN, K_SPACE, KEYDOWN, KEYUP, QUIT, RESIZABLE, MOUSEBUTTONDOWN, MOUSEBUTTONUP
#from graphics2D import UDim2


# ----- # Import : Maths Libs # ----- #
from vector import Vector2   # My lib of Vector
# from typing import Literal
import math


# ----- # Varaible : Setup # ----- #
pygame.init()

__screen:pygame.Surface = None
__background = None
__resolution:Vector2 = None
__layers = []
hello = "Hello World !" # Test Variable

# ----- # Setting : Texts # ----- #
__defaultTextFront = "monospace"
__defaultTextSize = 16

# ----- # Setting : keybord # ----- #
__keybordType = "azerty"

# ----- # Setting : render# ----- #
__inversedAxeY = True
__renderAuto = True














# --- Initailisation of Screen --- #

# --- Resolution --- #
def setResolution(resolution=(600, 480)):
    global __screen, __background, __resolution, __layers
    __screen = pygame.display.set_mode(resolution)
    # __background = pygame.Surface(__screen.get_size(),pygame.SRCALPHA)
    # __background = __background.convert()
    __resolution = Vector2(resolution)

    for iLayer in __layers:
        iLayer.size = Vector2(resolution)

def setWindowResizable():
    pygame.display.set_mode(__resolution,RESIZABLE)

def getResolution():
    return __resolution

# --- Title --- #
def setTitle(title:str):
    pygame.display.set_caption(str(title))

# --- Render Auto --- #
def set_autoRender(value:bool):
    global __renderAuto
    __renderAuto = value
def set_autoRender_Disable():
    set_autoRender(False)
def set_autoRender_Unable():
    set_autoRender(True)

# --- Init --- #
def init(resolution,title="New"):
    setResolution(resolution)
    setTitle(title)

def ifNotQuit():
    oneCycle()
    return True

def waitQuit():
    while ifNotQuit():
        pass


# --- Fronts --- #
def front_get():
    return __defaultTextFront
def front_reset():
    global __defaultTextFront
    __defaultTextFront = "monospace"
def front_set(frontName="monospace"):
    global __defaultTextFront
    __defaultTextFront = frontName
def front_size_get():
    return __defaultTextSize
def front_size_set(newFrontSize=16):
    global __defaultTextSize
    __defaultTextSize = newFrontSize



# --- Layers --- #
class Layer:
    def __init__(self,size) -> None:
        self.pos = Vector2(0,0)
        self.size = Vector2(size)
        self.zoom = 1
        # print("Layer:",size,self.size)
        self.surface = pygame.Surface((self.size.x,self.size.y),pygame.SRCALPHA)
        self.alpha = 255

        self.renderedDone = True

    def size_set(self,newSize):
        self.size = Vector2(newSize)
    def size_get(self):
        return self.size
    
    def position_set(self,newposition):
        self.pos = Vector2(newposition)
    def position_add(self,newposition):
        self.pos += Vector2(newposition)
    def position_get(self):
        return self.pos
    
    def zoom_set(self,newZoom):
        self.zoom = newZoom
        self.surface = image_resize(self.surface, self.size*self.zoom )
    def zoom_get(self):
        return self.zoom

    def alphaLevel_set(self,newAlpha):
        self.alpha = newAlpha
        self.surface.set_alpha(newAlpha)
    def alphaLevel_get(self):
        return self.alpha
    
    def surface_get(self) -> pygame.Surface:
        return self.surface
    def surface_new(self):
        self.surface = pygame.Surface((self.size.x,self.size.y),pygame.SRCALPHA)
    def surface_set(self,newSurface:pygame.Surface):
        self.surface = newSurface
    def surface_get_renderAuto(self):
        return self.surface_get()

    def surface_fill(self,color=(0,0,0,255)):
        self.surface.fill(color)
    def surface_clear(self):
        self.surface_fill((0,0,0,0))

# class LayerMini(Layer):



def layer_new():
    global __layers, __screen
    newLayer = Layer(__screen.get_size())
    __layers.append(newLayer)
    return newLayer

def newLayer():
    return layer_new()

def layer_add(layer):
    global __layers
    __layers.append(layer)

def layer_setLevelAlpha(layer:Layer,alpha:int):
    layer.surface.set_alpha(alpha)

def layer_get(number):
    return __layers[number]









# --- Layer : Vectoriel --- #
class VectorielLayer(Layer):
    def __init__(self,size) -> None:
        super().__init__(size)
        self.objects = list()

    def objects_add(self,newObject):
        self.objects.append(newObject)
    def objects_clear(self):
        self.objects = list()
    def surface_get(self) -> pygame.Surface:
        self.surface = pygame.Surface(self.size,pygame.SRCALPHA)
        for iObject in self.objects:
            iObject.draw(self)
        self.surface.set_alpha(self.alpha)
        return self.surface

def layer_newVectorielLayer():
    global __layers, __screen
    newLayer = VectorielLayer(__screen.get_size())
    __layers.append(newLayer)
    return newLayer

def newVectorielLayer():
    return layer_newVectorielLayer()














# --- Layer : Gui --- #
class GuiLayer(VectorielLayer):
    """ Can always contain Gui Object
        Example Structure : 
          GuiLayer 
            -> Gui_Frame    (basic contener)
                -> Gui_Silder
                -> Gui Button
                -> Gui_Frame  (can recreate a Frame in Frame)
            -> Gui_Frame_Scrollable   (advanced contener)
                -> Gui_Frame
                    -> Gui Button
                -> Gui Button
            -> Gui_Frame_Drawable (for use vectoriel object)
                -> VecText
                -> VecCercle
    """
    def __init__(self, size) -> None:
        super().__init__(size)

    def parent_get(self):
        """ If a children ask the grandmother, Gui Layer repond None """
        return None
    
    def detectIfOneButtonClic(self,mousePosition:Vector2):
        for i, iButton in enumerate(self.objects):
            if iButton.insidePos(mousePosition):
                if iButton.functionCall != None:
                    if iButton.functionParameters != None:
                        iButton.functionCall(iButton.functionParameters)
                    else:
                        iButton.functionCall()
                    return iButton
    def objects_add(self,newObject,functionCall=None,functionParameters=None):
        # if functionCall!= None:
        newObject.setupButton(functionCall,functionParameters)
        newObject.parent_set(self)
        # if isinstance(newObject.pos,UDim2):
        #     newObject.pos.setParent(self)
        # if isinstance(newObject.size,UDim2):
        #     newObject.size.setParent(self)
        self.objects.append(newObject)

        self.renderedDone = False # For the next Render Of Screen, do a render
        return newObject
    

    def surface_render(self):
        self.surface = pygame.Surface(self.size,pygame.SRCALPHA)
        for iObject in self.objects:
            surfaceIObject = iObject.surface_get() # If error : The object was not a Gui object
            if surfaceIObject: 
                self.surface.blit(surfaceIObject,iObject.pos.vector2())
        self.surface.set_alpha(self.alpha)
        
        self.renderedDone = True
        return self.surface
    
    def surface_render_all(self):
        for iObject in self.objects:
            iObject.surface_render_all()
        self.surface_render()
    
    def surface_autoRender(self):
        if not(self.renderedDone):
            # print("Gui Layer Render : Self")
            for iObject in self.objects:
                if not(iObject.renderedDone):
                    # print("Gui Layer Render : Object")
                    iObject.surface_autoRender()        
            self.surface_render()
            

    def surface_get(self) -> pygame.Surface:
        return self.surface
    
    def surface_get_renderAuto(self) -> pygame.surface:
        self.surface_autoRender()
        return self.surface
    

    def childrenWantRender(self):
        self.renderedDone = False


guiLayerList:list = list()

def layer_newGuiLayer():
    global guiLayerList
    newGuiLayer = GuiLayer(getResolution())
    guiLayerList.append(newGuiLayer)
    layer_add(newGuiLayer)
    return newGuiLayer

def addNewGuiObject(layer:GuiLayer,newObject):
    if isinstance(layer,GuiLayer):
        layer.objects_add(newObject)
    else:
        print("Error: The 'layer' are not a Gui Layer :",layer)
















# --- draw Function --- #
def fill(layer:Layer=__screen,color=(255,255,255,0)):
    layer.surface.fill(color)

def setBackgroundColor(color=(255,255,255,255)):
    __screen.fill(color)

def translatePoint(pos):
    # if __inversedAxeY:
    #     return (pos[0],__resolution[1]-pos[1])
    # else:
        return tuple(pos)

def draw_pixel(layer:Layer,pos,color):
    layer.surface.set_at(translatePoint(pos), color)

def draw_line(layer:Layer,pointA,pointB, color,thickness):
    pygame.draw.line(layer.surface, color, pointA, pointB, thickness)

def draw_rect_empty(layer:Layer,pos,size,color,thickness=1):
    pygame.draw.rect(layer.surface,color,pygame.Rect(translatePoint(pos),(size[0],size[1])),thickness)
def draw_rect(layer:Layer,pos,size,color):
    draw_rect_empty(layer,pos,size,color,0)

def draw_rectangle_empty(layer:Layer,pointA,pointB,color,thickness=1):
    position = (min(pointA[0],pointB[0]) , max(pointA[1],pointB[1]))
    size     =  max(pointA[0],pointB[0]) - min(pointA[0],pointB[0]) , max(pointA[1],pointB[1]) - min(pointA[1],pointB[1])
    draw_rect_empty(layer,position,size,color,thickness)
def draw_rectangle(layer:Layer,pointA,pointB,color):
    draw_rectangle_empty(layer,pointA,pointB,color,0)

def draw_cercle_empty(layer:Layer,centre,rayon,color,thickness=1):
    pygame.draw.circle(layer.surface,color,translatePoint(centre),rayon,thickness)
def draw_cercle(layer:Layer,centre,rayon,color):
    draw_cercle_empty(layer,centre,rayon,color,0)
    
def draw_polygon_empty(layer:Layer,pointList,color,thickness=1):
    pygame.draw.polygon(layer.surface,color,[translatePoint(ipoint) for ipoint in pointList],thickness)
def draw_polygon(layer:Layer,pointList,color):
    draw_polygon_empty(layer,pointList,color,0)

def draw_triangle_empty(layer:Layer,pointA,pointB,pointC,color,thickness=1):
    draw_polygon_empty(layer,[pointA,pointB,pointC],color,thickness)
def draw_triangle(layer:Layer,pointA,pointB,pointC,color):
    draw_polygon(layer,[pointA,pointB,pointC],color)

def draw_arc_empty(layer:Layer,position,rayon,startAngle,endAngle,color,thickness=1):
    rectangle = pygame.Rect(position,(2*rayon,2*rayon))
    pygame.draw.arc(layer.surface,color,rectangle,startAngle,endAngle,thickness)
def draw_arc_empty_degre(layer:Layer,position,rayon,startAngle,endAngle,color,thickness=1):
    draw_arc_empty(layer,position,rayon,startAngle* math.pi / 180,endAngle* math.pi / 180,color,thickness)
def draw_arc(layer:Layer,position,rayon,startAngle,endAngle,color):
    draw_arc_empty(layer,position,rayon,startAngle,endAngle,color,0)
def draw_arc_degre(layer:Layer,position,rayon,startAngle,endAngle,color):
    draw_arc(layer,position,rayon,startAngle,endAngle,color,0)















# --- Vectoriel --- #
def addNewObjectVectoriel(layer:VectorielLayer,newObject):
    if isinstance(layer,VectorielLayer):
        layer.objects_add(newObject)
    else:
        print("Error: The 'layer' are not a layer :",layer)


def detection_rect(button,mousePos:Vector2) -> bool:
    return mousePos.x>= button.pos.x and mousePos.y>= button.pos.y and mousePos.x<button.pos.x+button.size.x and mousePos.y<button.pos.y+button.size.y
def detection_rectangle(button,mousePos:Vector2) -> bool:
    return mousePos.x>= button.posA.x and mousePos.y>= button.posA.y and mousePos.x<button.posB.x and mousePos.y<button.posB.y
def detection_circular(button,mousePos:Vector2) -> bool:
    return (button.center.x-mousePos.x)**2 + (button.center.y-mousePos.y)**2 <= button.rayon


class BasicVectoriel:
    def __init__(self,color,thickness,fillColor):
        self.color = color
        self.thickness = thickness
        self.fillColor = fillColor
    def setupButton(self,functionCall,functionParameters=None):
        self.functionCall = functionCall
        self.functionParameters = functionParameters
    def insidePos(self,pos:Vector2):
        print("Warn : it is the 'BasicVectoriel' detection insidePos ")
        return False

def button_add(vectorielObject:BasicVectoriel,functionCall,functionParameters=None):
    return vectorielObject.setupButton(functionCall,functionParameters)



class VecLine():
    def __init__(self,pointA,pointB,color,thickness) -> None:
        self.pointA = pointA
        self.pointB = pointB
        self.color = color
        self.thickness = thickness
    def draw(self,layer):
        draw_line(layer,self.pointA.vector2(),self.pointB.vector2(),self.color,self.thickness)
    def insidePos(self,pos:Vector2):
        return False

class VecRect(BasicVectoriel):
    def __init__(self,pos,size,color,fillColor,thickness) -> None:
        super().__init__(color, thickness, fillColor)
        self.pos:Vector2 = pos
        self.size:Vector2 = size
    def draw(self,layer):
        if self.fillColor:
            draw_rect(layer,self.pos.vector2(),self.size.vector2(),self.fillColor)
        if self.thickness != 0 and self.color:
            draw_rect_empty(layer,self.pos.vector2(),self.size.vector2(),self.color,self.thickness)
    def insidePos(self,pos:Vector2):
        return detection_rect(self,pos)

class VecRectangle(BasicVectoriel):
    def __init__(self,posA,posB,color,fillColor,thickness) -> None:
        super().__init__(color, thickness, fillColor)
        self.posA = posA
        self.posB = posB
    def draw(self,layer):
        if self.fillColor:
            draw_rectangle(layer,self.posA.vector2(),self.posB.vector2(),self.fillColor)
        if self.thickness != 0 and self.color:
            draw_rectangle_empty(layer,self.posA.vector2(),self.posB.vector2(),self.color,self.thickness)
    def insidePos(self,pos:Vector2):
        return detection_rectangle(self,pos)

class VecCercle(BasicVectoriel):
    def __init__(self,center,rayon,color,fillColor,thickness) -> None:
        super().__init__(color, thickness, fillColor)
        self.center = center
        self.rayon = rayon
    def draw(self,layer):
        if self.fillColor:
            draw_cercle(layer,self.center.vector2(),self.rayon,self.fillColor)
        if self.thickness != 0 and self.color:
            draw_cercle_empty(layer,self.center.vector2(),self.rayon,self.color,self.thickness)
    def insidePos(self,pos:Vector2):
        return detection_circular(self,pos)

class VecPolygone(BasicVectoriel):
    def __init__(self,points,rayon,color,fillColor,thickness) -> None:
        super().__init__(color, thickness, fillColor)
        self.points = points
    def draw(self,layer):
        if self.fillColor:
            draw_polygon(layer,self.points,self.fillColor)
        if self.thickness != 0 and self.color:
            draw_polygon_empty(layer,self.points,self.color,self.thickness)
    def insidePos(self,pos:Vector2):
        return False

class VecTriangle(BasicVectoriel):
    def __init__(self,pointA,pointB,pointC,color,fillColor,thickness) -> None:
        super().__init__(color, thickness, fillColor)
        self.pointA = pointA
        self.pointB = pointB
        self.pointC = pointC
    def draw(self,layer):
        if self.fillColor:
            draw_triangle(layer,self.pointA,self.pointB,self.pointC,self.fillColor)
        if self.thickness != 0 and self.color:
            draw_triangle_empty(layer,self.pointA,self.pointB,self.pointC,self.color,self.thickness)
    def insidePos(self,pos:Vector2):
        return False
    









# --- Image --- #

def image_paste(layer,pathImage,position):
    pos = translatePoint(position)
    image = pygame.image.load(pathImage).convert()
    layer.surface.blit(image,pos)

def image_paste_RGBA(layer,pathImage,position):
    pos = translatePoint(position)
    image = pygame.image.load(pathImage).convert_alpha()
    layer.surface.blit(image,pos)
    

def imageLoaded_pasteTo_imageLoaded(surface,image:pygame.Surface,position):
    pos = translatePoint(position)
    surface.blit(image.convert(),pos)

def imageLoaded_pasteTo_imageLoadedRGBA(surface,image:pygame.Surface,position):
    pos = translatePoint(position)
    surface.blit(image.convert_alpha(),pos)


def imageLoaded_paste(layer,image:pygame.Surface,position):
    imageLoaded_pasteTo_imageLoaded(layer.surface,image,position)

def imageLoaded_paste_RGBA(layer,image,position):
    imageLoaded_pasteTo_imageLoadedRGBA(layer.surface,image,position)
    

def image_paste_comeOpenCV_BGR(layer,openCVImage,position):
    pos = translatePoint(position)
    pygameImage = pygame.image.frombuffer(openCVImage.tostring(), openCVImage.shape[1::-1], "BGR")
    layer.surface.blit(pygameImage,pos)

def image_paste_comeOpenCV_ABGR(layer,openCVImage,position): # Ne marche pas
    pos = translatePoint(position)
    pygameImage = pygame.image.frombuffer(openCVImage.tostring(), openCVImage.shape[1::-1], "BGRA")
    layer.surface.blit(pygameImage,pos)

def image_concatenation(imageA:pygame.Surface,imageB:pygame.Surface):
    newImage = imageA.copy()
    newImage.blit(imageB,(0,0)) # blit return a Rect Object (not a Surface)
    return newImage

def image_load(imageName):
    return pygame.image.load(imageName).convert()

def image_load_RGBA(imageName):
    return pygame.image.load(imageName).convert_alpha()

def image_getSize(image:pygame.Surface):
    return image.get_size()

def image_resize(image:pygame.Surface,newSize:Vector2):
    return pygame.transform.scale(image, newSize)

def image_fill_withParten(image:pygame.Surface,partern:pygame.Surface):
    sizePaterne = partern.get_size()
    sizeImage = image.get_size()
    for iy in range(sizeImage[1]//sizePaterne[1]):
        for ix in range(sizeImage[0]//sizePaterne[0]):
            image.blit(partern,(ix*sizePaterne[0],iy*sizePaterne[1]))
    return image



def image_clip(surface, pos:Vector2, size:Vector2):
    x, y = pos
    x_size, y_size = size
    """ Get a part of the image """
    handle_surface = surface.copy()                       # Sprite that will get process later
    clipRect = pygame.Rect(x,y,x_size,y_size)             # Part of the image
    handle_surface.set_clip(clipRect)                     # Clip or you can call cropped
    image = surface.subsurface(handle_surface.get_clip()) # Get subsurface
    return image



class VecImage(BasicVectoriel):
    def __init__(self, imageName, pos:Vector2, color, thickness, fillColor):
        super().__init__(color, thickness, fillColor)
        self.imagePath = imageName
        self.image = pygame.image.load(imageName).convert()
        self.size = Vector2(self.image.get_size())
        self.pos = pos
    def draw(self,layer):
        layer.blit(self.image) #  /!\ Attention a finir 
    def insidePos(self,pos:Vector2):
        return detection_rect(self,pos)










# --- Text --- #
def getTextSize(text:str,front,sizeFront):
    myfont = pygame.font.SysFont(front, sizeFront)
    return myfont.size(text)
def getTextHeight(text:str,front,sizeFront):
    return getTextSize(text,front,sizeFront)[0]
def getTextWidth(text:str,front,sizeFront):
    return getTextSize(text,front,sizeFront)[1]

def draw_textComplexe(layer:Layer,text, pos, center=Vector2(0,0), color=(255,255,255,255), fillColor=None, frontSize=__defaultTextSize, front=__defaultTextFront):
    frontSize= __defaultTextSize if frontSize == None else frontSize
    front= __defaultTextFront if front == None else front
    sizeText = getTextSize(text,front,frontSize)
    myfont = pygame.font.SysFont(front, frontSize,bold=True)
    label = myfont.render(text, 1, color)
    position = translatePoint((pos[0] - sizeText[0]*center[0] ,pos[1] - sizeText[1]*center[1]))
    if fillColor:
        draw_rect(layer,position,sizeText,fillColor)
    layer.surface.blit(label, position)
def draw_text(layer,text, pos, color=(255,255,255,255), frontSize=__defaultTextSize, front=__defaultTextFront):
    draw_textComplexe(layer,text, pos, Vector2(0,0), color, None, frontSize, front)
def draw_textCenter(layer,text, pos, color=(255,255,255,255), frontSize=__defaultTextSize, front=__defaultTextFront):
    draw_textComplexe(layer,text, pos, Vector2(0.5,0.5), color, None, frontSize, front)

class VecText(BasicVectoriel):
    def __init__(self, text, pos, color=(255,255,255,255), fillColor=None, frontSize=None, center=Vector2(0,0), front=None):
        super().__init__(color, None, fillColor)
        self.text:str = text
        self.pos:Vector2 = pos
        self.center:Vector2 = center
        self.frontSize:int = frontSize
        self.front:str  = front
    def draw(self,layer):
        draw_textComplexe(layer,self.text,self.pos,self.center,self.color,self.fillColor,self.frontSize,self.front)
    def insidePos(self,pos:Vector2):
        return False
    
class VecText_Framed(VecText):
    def __init__(self, text, pos, size, colorText=(255, 255, 255, 255), colorFrame=None, fillColor=None, thickness=2, frontSize=None, center=Vector2(0.5, 0.5), front=None):
        super().__init__(text, pos, colorText, fillColor, frontSize, center, front)
        self.size = size
        self.colorFrame = colorFrame
        self.thickness = thickness
    def draw(self,layer):
        if self.fillColor and self.fillColor[3] != 0:
            draw_rect(layer,self.pos,self.size,self.fillColor)
        if self.thickness != 0 and self.colorFrame:
            draw_rect_empty(layer, self.pos.vector2(), self.size.vector2(), self.colorFrame, self.thickness)
        # print(">",self.center)            
        draw_textComplexe(layer,self.text, self.pos+self.size//2 ,self.center,self.color,self.fillColor,self.frontSize,self.front)
    def insidePos(self,pos:Vector2):
        return detection_rect(self,pos)
















# --- Stange Gui Objects --- #
class UDim2:
    def __init__(self,xScale: float, xOffset: int, yScale: float, yOffset: int) -> None:
        self.new(xScale,xOffset, yScale, yOffset)
    def new(self,xScale: float, xOffset: int, yScale: float, yOffset: int):
        self.Offset = Vector2(xOffset,yOffset)
        self.Scale = Vector2(xScale,yScale)

        self.parent = None
        self.updateData()
        return self
    
    def updateData(self):
        self.xOffset = self.Offset.x
        self.yOffset = self.Offset.y
        self.xScale = self.Scale.x
        self.yScale = self.Scale.y
        
        if self.parent != None:
            self.x = self.parent.size.x*self.xScale + self.xOffset    # Width = X: UDim # The x dimension scale and offset of the UDim2.
            self.y = self.parent.size.y*self.yScale + self.yOffset    # Height = Y: UDim # The y dimension scale and offset of the UDim2.
        else:
            self.x = self.xOffset
            self.y = self.yOffset
        self.absolue = Vector2(self.x,self.y)

    def setParent(self,newParent):
        self.parent = newParent
        self.updateData()
    
    def setScale(self,xScale: float, yScale: float):
        self.Scale = Vector2(xScale,yScale)
        self.updateData()

    def setOffset(self,xOffset: int, yOffset: int):
        self.Offset = Vector2(xOffset,yOffset)
        self.updateData()

    def vector2(self):
        return self.absolue

    # Consol #
    def __repr__(self):  
        """Afiche in console"""
        return "UDim2({},{} ; {},{},{},{})".format(self.x,self.y,self.Scale.x,self.Offset.x,self.Scale.y,self.Offset.y)
    
    # List Comportement 
    def __getitem__(self, i):
        return (self.x,self.y,self.Scale.x,self.Offset.x,self.Scale.y,self.Offset.y)[i]
    def __setitem__(self, i, v):
        if i ==0:
            self.x = v
        elif i ==1:
            self.y = v
        elif i ==2:
            self.Scale.x = v
        elif i ==3:
            self.Offset.x = v
        elif i ==4:
            self.Scale.y = v
        elif i ==5:
            self.Offset.y = v
    def __len__(self):
        return 6
    
    def __add__(self, other):
        """ Addition Operation (+)"""
        if isinstance(other, Vector2) or isinstance(other, UDim2):
            return Vector2(self.x + other.x, self.y+ other.y)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector2(self.x+ other, self.y+ other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 2:
                return Vector2(self.x+ other[0], self.y+ other[1])
        print("Error UDim2 operation (+) null")
    def __sub__(self, other):
        """ Remove Operation (-)"""
        if isinstance(other, Vector2) or isinstance(other, UDim2):
            return Vector2(self.x - other.x, self.y- other.y)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector2(self.x- other, self.y- other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 2:
                return Vector2(self.x- other[0], self.y- other[1])
        print("Error UDim2 operation (-) null")
    def __mul__(self, other):
        """ Multi Operation (*)"""
        if isinstance(other, Vector2) or isinstance(other, UDim2):
            return Vector2(self.x * other.x, self.y * other.y)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector2(self.x* other, self.y* other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 2:
                return Vector2(self.x* other[0], self.y* other[1])
        print("Error UDim2 operation (*) null")
    def __truediv__(self, other):
        """ Basic Divition Operation (/)"""
        if isinstance(other, Vector2) or isinstance(other, UDim2):
            return Vector2(self.x / other.x, self.y / other.y)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector2(self.x/ other, self.y/ other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 2:
                return Vector2(self.x/ other[0], self.y/ other[1])
        print("Error UDim2 operation (/) null")
    def __floordiv__(self, other):
        """ Floor Divition Operation (//)"""
        if isinstance(other, Vector2) or isinstance(other, UDim2):
            return Vector2(self.x // other.x, self.y // other.y)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector2(self.x// other, self.y// other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 2:
                return Vector2(self.x// other[0], self.y// other[1])
        print("Error UDim2 operation (//) null")
    def __mod__(self, other):
        """ Modulo Operation (%)"""
        if isinstance(other, Vector2) or isinstance(other, UDim2):
            return Vector2(self.x % other.x, self.y % other.y)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector2(self.x% other, self.y% other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 2:
                return Vector2(self.x% other[0], self.y% other[1])
        print("Error UDim2 operation (%) null")
    def __pow__(self, other):
        """ Power Operation (**)"""
        if isinstance(other, Vector2) or isinstance(other, UDim2):
            return Vector2(self.x ** other.x, self.y ** other.y)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector2(self.x** other, self.y** other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 2:
                return Vector2(self.x** other[0], self.y** other[1])
        print("Error UDim2 operation (**) null")


















class Gui_Object(BasicVectoriel):
    def __init__(self, pos:UDim2, size:UDim2, color, fillColor, thickness, pivot=Vector2(0,0)) -> None:
        super().__init__(color, thickness, fillColor)

        self.pos:UDim2 = pos
        self.size:UDim2 = size
        self.pivot:Vector2 = pivot
        self.z = 1  # Zoom or Axe Z in Vector3

        self.color = color
        self.parent:Gui_Frame = None

        self.surface_new()
        
        self.visible = True
        self.alpha = 255

        self.renderedDone = False # True if the surface was current
        
    # ----------------------------#

    # --- # Detection Button # --- #
    def insidePos(self,pos:Vector2):
        """ Return if the position are in object or not 
             -> Detection of Button Clic                """
        return detection_rect(self,pos)
        # return detection_rectangle(self,pos)
        # return detection_circular(self,pos)
        # return False

    # ----------------------------#
       
    # --- # Hide / Show # --- #
    def show(self):
        """ show the object / set visible for next render """
        if not(self.visible_get()):
            self.visible = True
            self.wantRenderParent()
    def hide(self):
        """ hide the object / set invisible for next render """
        if self.visible_get():
            self.visible = False
            self.wantRenderParent()
    def visible_set(self,stat:bool):
        """ show or hide the object to the next render """
        if self.visible != stat:
            self.visible = stat
            self.wantRenderParent()
    def visible_get(self):
        """ return if the object will show or hide, the next render """
        return self.visible

    # --- # Pos # --- #
    def pos_set(self,newPos):
        """ Position of Object, relatif of Parent """
        self.pos = newPos
        self.wantRenderParent()
    def pos_add(self,newPos):
        """ Position of Object, relatif of Parent """
        self.pos += newPos
        self.wantRenderParent()
    def pos_get(self):
        """ Position of Object, relatif of Parent """
        return self.size
    
    # --- # Size # --- #
    def size_set(self,newSize):
        """ Size of Object, relatif of Parent """
        self.size = newSize
        self.wantRenderParent()
    def size_add(self,newSize):
        """ Size of Object, relatif of Parent """
        self.size += newSize
        self.wantRenderParent()
    def size_get(self):
        """ Size of Object, relatif of Parent """
        return self.size
    
    # --- # Zoom # --- #
    def zoom_set(self,newZoom):
        """ Coef of multiplicator of size """
        self.z = newZoom
        self.wantRenderParent()
    def zoom_get(self):
        """ Return Coef of multiplicator of size """
        return self.z
    
    # --- # Parent Function # --- #
    def parent_updateParentReference(self):
        if isinstance(self.pos,UDim2):
            self.pos.setParent(self.parent)
        if isinstance(self.size,UDim2):
            self.size.setParent(self.parent)
    def parent_set(self,newParent):
        """ Deini the parent / repair / origine / model of Object"""
        self.parent = newParent
        self.parent_of_parentChange()
        self.wantRenderParent()
    def parent_of_parentChange(self):
        """ Update Parent of All Children (Nothing because no children)"""
        self.parent_updateParentReference()
        self.wantRenderParent()
    def parent_get(self):
        """ Return the parent / repair / origine / model of Object"""
        return self.parent
    
    def getAncestor(self):
        """ Retunr list of parent of parent"""
        mother = list()
        iParent = self.parent_get()
        while iParent:
            # print("->",iParent.__class__.__name__)
            mother.append(iParent)
            iParent = iParent.parent_get()
        return mother
    def getMyDescendants(self)-> str:
        s = "> Game\Screen\GuiLayer"
        for i in self.getAncestor():
            s += "\\"+str(i.__class__.__name__)
        return s

    
    def wantRenderParent(self):
        self.renderedDone = False
        if self.parent_get():
            self.parent.childrenWantRender()

    # --- # Alpha Function # --- #
    def alphaLevel_set(self,newAlpha):
        """ Set the transparence of Object"""
        self.alpha = newAlpha
        self.surface.set_alpha(newAlpha)
        self.wantRenderParent()
    def alphaLevel_get(self):
        """ Get the transparence of Object"""
        return self.alpha
    
    # --- # Pivot Function # --- #
    def pivotPoint_set(self,newPivotPoint=Vector2(0,0)):
        """ Defini the point of origine of Object """
        self.pivot = newPivotPoint
        if isinstance(newPivotPoint,UDim2): # Pas de logique d'utilisé un UDim2
            self.pivot.setParent(self)
        self.wantRenderParent()
    def pivotPoint_get(self):
        """ Return the point of origine of Object """
        return self.pivot
    
    # ----------------------------#
    
    # --- # Render Function # --- #
    def surface_render(self):
        """ Render Surface of self """
        self.surface_new()
        if self.fillColor:
            self.surface.fill(self.fillColor)
        if self.fillColor:
            draw_rect_empty(self,Vector2(0,0),self.size,self.color,self.thickness)
        self.renderedDone = True

    def surface_render_all(self):
        """ Render Surface of all Childrens and self"""
        self.surface_render()
        
    def surface_autoRender(self):
        # print(self.getMyDescendants())
        if not(self.renderedDone):    
            self.surface_render()
        
    def surface_get(self) -> pygame.Surface:
        """ Render the "image of button" """
        return self.surface
    
    def surface_new(self) -> pygame.Surface:
        """ Recreate new Surface """
        if self.size.x>0 and self.size.y>0:
            self.surface = pygame.Surface((self.size.x,self.size.y),pygame.SRCALPHA)
        self.wantRenderParent()
    
    def observable(self,cameraPos,parentSize, scale = 1):
        # parentPos = cameraPos
        """ Return if the Object was visible in his parent """
        # return (parentPos.x+parentSize.x > self.pos.x and self.pos.x+self.size.x > parentPos.x and parentPos.y+parentSize.y > self.pos.y and self.pos.y+self.size.y > parentPos.y) 
        #return (cameraPos.x + self.pos.x < parentSize.x and cameraPos.x +self.pos.x + self.size.x*scale > 0 and cameraPos.y +self.pos.y < parentSize.y and cameraPos.y +self.pos.y + self.size.y*scale > 0) 
        #return (cameraPos.x + self.pos.x < parentSize.x*scale and cameraPos.x +self.pos.x + self.size.x*scale > 0 and cameraPos.y +self.pos.y < parentSize.y*scale and cameraPos.y +self.pos.y + self.size.y*scale > 0)      
        # print(().x >0 ,  < parentSize.x*scale)
        # print(  (self.pos.x*scale - self.size.x*scale) > cameraPos.x  , cameraPos.x+(self.pos.x - cameraPos.x)*scale < parentSize.x)
        posA = self.pos*scale + cameraPos  + self.size.vector2()*scale
        posB = self.pos*scale + cameraPos
        return posA.x >0 and posA.y > 0 and posB.x < parentSize.x*scale and posB.y < parentSize.y*scale


    def draw(self,layer:pygame.Surface):
        if self.visible:
            self.surface_render()
            layer.surface.blit(self.surface, self.pos.vector2()-self.pivot.vector2())


class Gui_TextLabel(Gui_Object):
    def __init__(self, text:str, pos: UDim2, color=None, textColor=None, fillColor=None, frontSize:int=None, thickness:int=2, textCenter=Vector2(0.5,0.5),front=None, pivot=Vector2(0,0)) -> None:
        super().__init__(pos, Vector2(0,0), color, fillColor, thickness, pivot)

        self.text = text
        self.textColor = textColor
        self.textCenter = Vector2(0,0) if textCenter else textCenter
        self.frontSize= front_size_get() if frontSize == None else frontSize
        self.front= front_get() if front == None else front

    # --- # Button Dectection # --- #
    def insidePos(self,pos:Vector2):
        return False
    
    # --- # Text Function # --- #
    def text_set(self,newText:str):
        self.text = newText
        self.wantRenderParent()
    def text_get(self):
        return self.text
    
    def frontSize_set(self,newSize:int):
        self.frontSize = newSize
        self.wantRenderParent()
    def frontSize_get(self):
        return self.frontSize
    
    # --- # Render Function # --- #
    def surface_render(self):
        self.size = Vector2(getTextSize(self.text, self.front, self.frontSize))
        self.surface = pygame.Surface((self.size.x,self.size.y),pygame.SRCALPHA)
        if self.fillColor:
            self.surface.fill(self.fillColor)
        if self.text and self.textColor:
            # *self.size*self.textCenter
            draw_textComplexe(self,self.text, self.textCenter, self.textCenter,self.textColor,self.fillColor,self.frontSize,self.front)
        if self.color:
            draw_rect_empty(self,Vector2(0,0),self.size,self.color,self.thickness)
        self.renderedDone = True

class Gui_Image(Gui_Object):
    def __init__(self, imageSource:str, pos: UDim2, size:UDim2=None, colorFrame=None, colorTransparence=None, frameThickness=2, pivot=Vector2(0, 0)) -> None:
        """ imageSource will be :
             -> A str of path of Image 
             -> A Pygame.Surface         
            size will be None 
             -> to set of size Image """
        self.size = size
        super().__init__(pos, self.size, colorFrame, colorTransparence, frameThickness, pivot)
        if imageSource:
            self.image_new(imageSource)
        else:
            self.imageSurface = None


    def image_new(self,imageSource):
        if isinstance(imageSource,pygame.Surface):
            self.imageSurface = imageSource
            # self.surface = imageSource
        else:
            self.path = imageSource

            self.imageSurface = image_load_RGBA(self.path)
            assert self.imageSurface, "Error : Image not find"
            self.imageSize = Vector2(self.imageSurface.get_size())

        if not(self.size):
            self.size = Vector2(self.imageSize.x,self.imageSize.y)
        self.wantRenderParent()
    def image_get(self):
        return self.imageSurface
    def image_path_get(self):
        return self.path
    def image_resize(self,newSize):
        self.size = newSize
        self.surface = image_resize(self.surface,newSize.vector2())
        self.wantRenderParent()
    def surface_render(self):
        if self.color:
            draw_rect_empty(self,Vector2(0,0),self.size,self.color,self.thickness)

        if self.imageSurface:
            self.surface = image_resize(self.imageSurface,self.size.vector2()*self.z)
        else:
            self.surface = pygame.Surface((self.size.x,self.size.y),pygame.SRCALPHA)
            if self.fillColor:
                self.surface.fill(self.fillColor)

        self.renderedDone = True

        


class Gui_TextButton(Gui_Object):
    def __init__(self, text:str, pos: UDim2, size:UDim2, color=None, textColor=None, fillColor=None, frontSize:int=None, thickness:int=2, textCenter=Vector2(0,0),front=None,pivot=Vector2(0,0)) -> None:
        super().__init__(pos, size, color, fillColor, thickness, pivot)

        self.text = text
        self.textColor = textColor
        self.textCenter = Vector2(0,0) if textCenter else textCenter
        self.frontSize= front_size_get() if frontSize == None else frontSize
        self.front= front_get() if front == None else front
    
    # --- # Button Dectection # --- #
    def insidePos(self,pos:Vector2):
        return detection_rect(self,pos)
    
    # --- # Text Function # --- #
    def text_set(self,newText:str):
        self.text = newText
        self.wantRenderParent()
    def text_get(self):
        return self.text
    
    # --- # Render Function # --- #
    def surface_render(self):
        self.surface = pygame.Surface((self.size.x,self.size.y),pygame.SRCALPHA)
        if self.fillColor:
            self.surface.fill(self.fillColor)
        if self.text and self.textColor:
            draw_textCenter(self, self.text, self.size//2, self.textColor, self.frontSize)
        if self.color:
            draw_rect_empty(self,Vector2(0,0),self.size,self.color,self.thickness)
        self.renderedDone = True

class Gui_Prefab_BoolButton(Gui_TextButton):
    def __init__(self, text: str, pos: UDim2, size: UDim2, color=None, textColor=None, fillColor=None, frontSize: int = None, thickness: int = 2, textCenter=Vector2(0, 0), front=None, pivot=Vector2(0, 0)) -> None:
        super().__init__(text, pos, size, color, textColor, fillColor, frontSize, thickness, textCenter, front, pivot)

        self.value = False

        self.offsetCursor = 8
        self.sizeCursor = self.size.y-self.offsetCursor-self.offsetCursor
        self.sizeCursor = Vector2(self.sizeCursor,self.sizeCursor)
        self.controler_proportion_update()
    
    # --- # Value Function # --- #
    def value_get(self):
        return self.value
    def value_set(self, newValue):
        self.value = newValue
        self.wantRenderParent()
    def value_invers(self):
        self.value = not(self.value)        

    # --- # Deco Function # --- #
    def controler_offsetCursor_set(self,newValue):
        self.offsetCursor = newValue
        self.wantRenderParent()
    def controler_sizeCursor_set(self,newValue:Vector2):
        self.sizeCursor = newValue
        self.wantRenderParent()
    def controler_proportion_update(self):
        self.controler_pos = Vector2(self.size.x-self.sizeCursor.x-self.offsetCursor, self.offsetCursor) 
        self.wantRenderParent()

    # --- # Render Function # --- #
    def surface_render(self):
        self.surface = pygame.Surface((self.size.x,self.size.y),pygame.SRCALPHA)
        if self.fillColor:
            self.surface.fill(self.fillColor)
        if self.text and self.textColor:
            draw_textComplexe(self, self.text + " : " + str(self.value), self.size.vector2().OnlyY()//2 + Vector2(self.size.y - self.frontSize,0), Vector2(0,0.5), self.textColor,None, self.frontSize)
        
       
        if self.color:
            if self.value:
                draw_rect(self,   self.controler_pos + 4, self.sizeCursor-self.offsetCursor,self.color)
            draw_rect_empty(self, Vector2(0,0),self.size,self.color,self.thickness)
            draw_rect_empty(self, self.controler_pos, self.sizeCursor,self.color,self.thickness)
        else:
            if self.value:
                draw_rect(self, self.controler_pos + 4 , self.sizeCursor-self.offsetCursor,(0,0,0,255))
        self.renderedDone = True
    
    # --- # Button Dectection # --- #
    def insidePos(self,pos:Vector2) -> bool:
        if detection_rect(self,pos):
            self.value_invers()
            self.surface_render()
            self.wantRenderParent()
            return True
        return False






class Gui_Prefab_Slider_X(Gui_Prefab_BoolButton):
    def __init__(self, text: str, pos: UDim2, size: UDim2, color=None, textColor=None, fillColor=None, color_Cursor=-1 , frontSize: int = None, thickness: int = 2, textCenter=Vector2(0, 0), front=None, pivot=Vector2(0, 0)) -> None:
        super().__init__(text, pos, size, color, textColor, fillColor, frontSize, thickness, textCenter, front, pivot)
        
        self.value = 0
        self.valueMax = 100
        self.valueMin = 0

        self.color_cursor = color if color_Cursor == -1 else color_Cursor

        self.functionCall = None
        self.functionParameters = None
    
    # --- # Value Functions # --- #
    def extrmum_set(self,min,max):
        self.valueMin = min
        self.valueMax = max
        self.wantRenderParent()
    def extrmum_min_set(self,min):
        self.valueMin = min
        self.wantRenderParent()
    def extrmum_max_set(self,max):
        self.valueMax = max
        self.wantRenderParent()

    def extrmum_get(self):
        return (self.valueMin,self.valueMax)
    def extrmum_min_get(self):
        return self.valueMin
    def extrmum_max_get(self):
        return self.valueMax

       
    # --- # Render Functions # --- # 
    def surface_render(self):
        self.surface = pygame.Surface((self.size.x,self.size.y),pygame.SRCALPHA)
        if self.fillColor:
            self.surface.fill(self.fillColor)
        if self.color_cursor:
            silderY = self.size.y//2 
            draw_line(self,Vector2(silderY,silderY),Vector2(self.size.x-silderY,silderY),self.color_cursor,self.thickness)
            silderX = int((self.value-self.valueMin)/(self.valueMax-self.valueMin)*(self.size.x-silderY-silderY)+silderY)
            draw_cercle(self,Vector2(silderX,silderY),int(self.size.y*0.36),self.color_cursor)
            # print(silderX,silderY)
        if self.text and self.textColor:
            draw_textComplexe(self, self.text + " : " + str(self.value)[:6], self.size.vector2().OnlyY()//2 + Vector2(self.size.y - self.frontSize,0), Vector2(0,0.5), self.textColor,None, self.frontSize)
        if self.color:
            draw_rect_empty(self, Vector2(0,0),self.size,self.color,self.thickness)
        self.renderedDone = True
    
    # --- # Button Dectection # --- #
    def insidePos(self,pos:Vector2):
        if detection_rect(self,pos+self.pivot.vector2()):
            # dA = pos.x -self.pos.x +self.pivot.vector2().x
            silderY = self.size.y//2 

            dA = pos.x-self.pos.x -self.size.x*self.pivot.x

            # self.value = self.valueMin + (self.valueMax-self.valueMin)* dA/self.size.x
            self.value = (self.valueMax - self.valueMin)* (dA-silderY)/(self.size.x-silderY-silderY) + self.valueMin
            # self.functionParameters = self.value
            if self.value < self.valueMin:
                self.value = self.valueMin
            if self.value > self.valueMax:
                self.value = self.valueMax

            # print(" > new Value :",self.value)
            self.surface_render()
            self.wantRenderParent()
            return True
        return False

class Gui_Prefab_Slider_Y (Gui_Prefab_Slider_X):
    def __init__(self, text: str, pos: UDim2, size: UDim2, color=None, textColor=None, fillColor=None, color_Cursor=-1, frontSize: int = None, thickness: int = 2, textCenter=Vector2(0, 0), front=None, pivot=Vector2(0, 0)) -> None:
        super().__init__(text, pos, size, color, textColor, fillColor, color_Cursor, frontSize, thickness, textCenter, front, pivot)

    # --- # Render Functions # --- # 
    def surface_render(self):
        self.surface = pygame.Surface((self.size.x,self.size.y),pygame.SRCALPHA)
        if self.fillColor:
            self.surface.fill(self.fillColor)
        if self.color_cursor:
            silderX = self.size.x//2 
            draw_line(self,Vector2(silderX,silderX),Vector2(silderX,self.size.y-silderX),self.color_cursor,self.thickness)
            silderY = int((self.value-self.valueMin)/(self.valueMax-self.valueMin)*(self.size.y-silderX-silderX)+silderX)
            draw_cercle(self,Vector2(silderX,silderY),int(self.size.x*0.36),self.color_cursor)
            # print(silderX,silderY)
        if self.text and self.textColor:
            draw_textComplexe(self, self.text + " : " + str(self.value)[:6], self.size.vector2().OnlyY()//2 + Vector2(self.size.y - self.frontSize,0), Vector2(0,0.5), self.textColor,None, self.frontSize)
        if self.color:
            draw_rect_empty(self, Vector2(0,0),self.size,self.color,self.thickness)
        self.renderedDone = True
            
    
    # --- # Button Dectection # --- #
    def insidePos(self,pos:Vector2):
        if detection_rect(self,pos+self.pivot.vector2()):
            # dA = pos.x -self.pos.x +self.pivot.vector2().x
            silderX = self.size.x//2 

            dA = pos.y-self.pos.y -self.size.y*self.pivot.y

            # self.value = self.valueMin + (self.valueMax-self.valueMin)* dA/self.size.x
            self.value = (self.valueMax - self.valueMin)* (dA-silderX)/(self.size.y-silderX-silderX) + self.valueMin
            # self.functionParameters = self.value
            if self.value < self.valueMin:
                self.value = self.valueMin
            if self.value > self.valueMax:
                self.value = self.valueMax

            # print(" > new Value :",self.value)
            self.surface_render()
            self.wantRenderParent()
            return True
        return False

















class Gui_Frame(Gui_Object):
    def __init__(self, pos: UDim2, size: UDim2, color, fillColor, thickness, pivot=Vector2(0,0)) -> None:
        super().__init__(pos, size, color, fillColor, thickness,pivot)

        self.objects = list()
    
    # --- # Objects List Function # --- #
    def objects_add(self,newObject:Gui_Object,functionCall=None,functionParameters=None):
        """ To add Object in Frame"""
        newObject.setupButton(functionCall,functionParameters) # Setup Button (same if is not)

        newObject.parent_set(self) # Set value "parent" of children to self

        self.objects.append(newObject) # Add to list of Objects of Frame

        self.childrenWantRender()
        return newObject
    def objects_clear(self):
        self.objects = list()
        self.childrenWantRender()
    def objects_get(self):
        return self.objects
    def objects_remove(self,objectToDelet):
        self.objects.remove(objectToDelet)
        self.childrenWantRender()

    def parent_of_parentChange(self):
        """ Update Parent of All Children"""
        self.parent_updateParentReference()
        for iObject in self.objects:
            iObject.parent_of_parentChange()

    def childrenWantRender(self):
        if not(self.renderedDone):
            self.renderedDone = False
        else:
            self.renderedDone = False
            self.wantRenderParent()
    
    # --- # UIListLayout Function # --- #
    def UIListLayout_setup(self,offset=0,padding=[0,0,0,0]):
        """ Automatic order the elements
             -> offset = space betewe two elements 
             -> padding = [Top, Bottom, Left, Right] """
        self.padding_Top = padding[0]
        self.padding_Bottom = padding[1]
        self.padding_Left = padding[2]
        self.padding_Right = padding[3]
        
        self.offset:int = offset
        self.fillDirection = "Y"

        self.allignementX:int = 0 # enter 0 et 1
        self.allignementY:int = 0 # enter 0 et 1

    def UIListLayout_order(self):
        if self.fillDirection == "Y":
            relatifPos = Vector2(self.padding_Right,self.padding_Top)
            if self.allignementX == 1:
                relatifPos = Vector2(self.padding_Left,self.padding_Top)
            for iObject in self.objects:
                iObject.pos_set(relatifPos.copy())
                relatifPos.y += iObject.size_get().y + self.offset
        elif self.fillDirection == "X":
            relatifPos = Vector2(self.padding_Right,self.padding_Top)
            if self.allignementY == 1:
                relatifPos = Vector2(self.padding_Right,self.padding_Bottom)
            for iObject in self.objects:
                iObject.pos_set(relatifPos.copy())
                relatifPos.x += iObject.size_get().x + self.offset
        self.wantRenderParent()
            
            


    # --- # Render Function # --- #
    def surface_render(self):
        """ Render Surface of self """
        self.surface = pygame.Surface((self.size.x, self.size.y),pygame.SRCALPHA)
        self.surface.set_alpha(self.alpha)
        
        if self.fillColor != None:
            self.surface.fill(self.fillColor)

        for iObject in self.objects:
            surfaceIObject = iObject.surface_get() # If error : The object was not a Gui object
            if surfaceIObject: 
                self.surface.blit(surfaceIObject,iObject.pos*self.z - iObject.pivot.vector2()*iObject.size.vector2())

        if self.z != 1:
            self.surface = image_resize(self.surface,self.size*self.z)

        if self.color != None:
            draw_rect_empty(self,Vector2(0,0),self.size * self.z, self.color,self.thickness)
        self.renderedDone = True
        return self.surface
    
    def surface_render_all(self):
        """ Render Surface of all Children and self"""
        for iObject in self.objects:
            iObject.surface_render_all()
        self.surface_render()
        
    def surface_autoRender(self):
        if not(self.renderedDone):
            for iObject in self.objects:
                if not(iObject.renderedDone):
                    iObject.surface_autoRender()        
            self.surface_render()

    # --- # Detection of Clic Function # --- #
    def insidePos(self,pos:Vector2):
        for iObject in self.objects:
            test = iObject.insidePos(pos-self.pos.vector2() + self.size*self.pivot.vector2())
            if test:
                self.functionCall = iObject.functionCall
                self.functionParameters = iObject.functionParameters
                return True
    

class Gui_Frame_BackgroundImage(Gui_Frame):
    def __init__(self, pos: UDim2, size: UDim2, backgroundImage, color, fillColor, thickness, pivot=Vector2(0, 0)) -> None:
        super().__init__(pos, size, color, fillColor, thickness, pivot)

        self.backgroundImage:pygame.Surface = backgroundImage
    
    def surface_render(self):
        """ Render Surface of self """
        self.surface = pygame.Surface((self.size.x, self.size.y),pygame.SRCALPHA)
        self.surface.set_alpha(self.alpha)
        
        if self.fillColor != None:
            self.surface.fill(self.fillColor)
            
        if self.backgroundImage:
            backgroundImage_resize = image_resize(self.backgroundImage,(self.backgroundImage.get_size()[0]*self.zoom_get(), self.backgroundImage.get_size()[1]*self.zoom_get()))
            backgroundSize = Vector2(backgroundImage_resize.get_size())
            for iy in range(int((self.size.y + backgroundSize[1]*2)// backgroundSize[1])):
                for ix in range(int((self.size.x + backgroundSize[0]*2)//backgroundSize[0])):
                    self.surface.blit(backgroundImage_resize,backgroundSize*Vector2(ix-1,iy-1))

        for iObject in self.objects:
            surfaceIObject = iObject.surface_get() # If error : The object was not a Gui object
            if surfaceIObject: 
                self.surface.blit(surfaceIObject,iObject.pos*self.z - iObject.pivot.vector2()*iObject.size.vector2())

        if self.z != 1:
            self.surface = image_resize(self.surface,self.size*self.z)

        if self.color != None:
            draw_rect_empty(self,Vector2(0,0),self.size * self.z, self.color,self.thickness)
        
        self.renderedDone = True
        return self.surface
    


class Gui_Frame_Scrollable(Gui_Frame):
    """ Can juste contain others Frame """
    def __init__(self, pos: UDim2, size: UDim2, color, fillColor, thickness) -> None:
        super().__init__(pos, size, color, fillColor, thickness)
        self.cameraPos = Vector2(0,0)
        # self.z = 1  # Zoom or Axe Z in Vector3

        self.backgroundImage:pygame.Surface = None
    


    # --- # Camera # --- #
    def camera_position_set(self,newPos:Vector2):
        self.cameraPos = newPos
        self.childrenWantRender()
    def camera_position_add(self,delta=Vector2(1,1)):
        self.cameraPos += delta
        self.childrenWantRender()
    def camera_position_get(self):
        return self.cameraPos

    def zoom_set(self,newZoom):
        if newZoom == 0:
            print("Error : Zoom was set to 0" )
            return 
        
        self.z = newZoom
        """ Last Zoom 
        for iObject in self.objects:
            iObject.zoom_set(newZoom)
            # print("Change zoom")
            # iObject.z = newZoom
            # print(" > ",iObject.z)
        # self.surface_render_all()
        """
        self.surface_render()
        self.childrenWantRender()
    def zoom_get(self):
        return self.z


    # --- # Background Image # --- #
    def backgroundImage_setup(self,newImage:pygame.Surface):
        self.backgroundImage = newImage


    # --- # Render Function # --- #
    def surface_render(self):
        self.surface = pygame.Surface((self.size.x//self.z,self.size.y//self.z),pygame.SRCALPHA)

        if self.backgroundImage:
            # backgroundImage_resize = image_resize(self.backgroundImage,(self.backgroundImage.get_size()[0]*self.zoom_get(), self.backgroundImage.get_size()[1]*self.zoom_get()))
            # backgroundSize = Vector2(backgroundImage_resize.get_size())

            backgroundSize = Vector2(self.backgroundImage.get_size())
            for iy in range(int((self.size.y + backgroundSize[1]*2)/ backgroundSize[1])):
                for ix in range(int((self.size.x + backgroundSize[0]*2)/backgroundSize[0])):
                    posBack = Vector2( self.cameraPos.x % backgroundSize[0], self.cameraPos.y % backgroundSize[1] )
                    # self.surface.blit(backgroundImage_resize,posBack + backgroundSize*Vector2(ix-1,iy-1))
                    self.surface.blit(self.backgroundImage,backgroundSize*Vector2(ix-1,iy-1)-posBack)

        # print("\n\n\n\n\n\n\n\n\n\n\n+ ------------ +")
        for iObject in self.objects:
            # if iObject.observable(self.cameraPos  ,self.size//self.z, self.z): #
                # print("   > Show ")

            # self.surface.blit(iObject.surface_get(), iObject.pos*self.z + self.cameraPos*self.z - iObject.pivot*iObject.size.vector2())
            # self.surface.blit(iObject.surface_get(), (iObject.pos -self.cameraPos ) )
            self.surface.blit(iObject.surface_get(), (iObject.pos -self.cameraPos) - iObject.pivot*iObject.size.vector2())
            """
            iObject.surface_render()
            surfaceIObject = iObject.surface_get() # If error : The object was not a Gui object
            print("Object :",surfaceIObject.get_size())
            if surfaceIObject: 
                surfaceVisibleObject = image_clip(surfaceIObject,iObject.pos*self.z + self.cameraPos - iObject.pivot*iObject.size.vector2(), self.size//self.z)
                self.surface.blit(surfaceVisibleObject, )
                # self.surface.blit(surfaceIObject, iObject.pos*self.z + self.cameraPos - iObject.pivot*iObject.size.vector2())
            """
        
        self.surface = image_resize(self.surface,self.size.vector2())

            # else:
            #     print("   > Hide ")
        if self.color != None:
            draw_rect_empty(self,Vector2(0,0),self.size*self.z, self.color,self.thickness)

        self.renderedDone = True
        # print("Scolable Frame : ",self.surface.get_size())
        return self.surface
    
    def surface_render_all(self):
        for iObject in self.objects:
            iObject.surface_render_all()
        return self.surface_render()

    def draw(self,layer:pygame.Surface):
        if self.visible:
            self.surface_render()
            layer.surface.blit(self.surface, self.pos.vector2()-self.pivot.vector2())


    # --- # Detection of Clic Function # --- #
    def insidePos(self,pos:Vector2):
        for iObject in self.objects:
            test = iObject.insidePos(pos-self.pos.vector2() + self.size*self.pivot.vector2() - self.cameraPos)
            if test:
                self.functionCall = iObject.functionCall
                self.functionParameters = iObject.functionParameters
                return True





class Gui_Frame_Drawable(Gui_Frame):
    def __init__(self, pos: UDim2, size: UDim2, color, fillColor, thickness, pivot=Vector2(0.5,0.5)) -> None:
        super().__init__(pos, size, color, fillColor, thickness,pivot)
        

    # --- # Function to delte # --- #
    # def surface_get(self) -> pygame.Surface:
    #     return self.surface
    # def surface_new(self):
    #     self.surface = pygame.Surface((self.size.x,self.size.y),pygame.SRCALPHA)
    # def surface_set(self,newSurface:pygame.Surface):
    #     self.surface = newSurface

    # def surface_fill(self,color=(0,0,0,255)):
    #     self.surface.fill(color)
    # def surface_clear(self):
    #     self.surface_fill((0,0,0,0))

    
    # --- # Render Function # --- #
    def surface_render(self):
        # self.surface = pygame.Surface((self.size.x, self.size.y),pygame.SRCALPHA)
        # if self.z != 1:
        # self.surface = image_resize(self.surface,self.size)

        for iObject in self.objects:
            iObject.draw(self)

        if self.surface.get_size()[0] != self.size.x*self.z:
            self.surface = image_resize(self.surface,self.size*self.z)
        
        if self.color != None:
            draw_rect_empty(self,Vector2(0,0),self.size*self.z, self.color,self.thickness)

        self.renderedDone = True
        return self.surface
    
    def surface_render_all(self):
        self.surface_render()




class Gui_Silder(Gui_Frame):
    def __init__(self, pos: UDim2, size: UDim2, color, fillColor, thickness=4) -> None:
        super().__init__(pos, size, color, fillColor, thickness)
        self.value = 0
        self.valueMax = 0
        self.valueMin = 100

        self.functionCall = None
        self.functionParameters = None
    
    def setValueTo(self,v):
        self.value = v
    def getValueTo(self,v):
        self.value = v

    def addlink_setCallFunction(self,callFunction,parameters=None):
        self.functionCall = callFunction
        self.functionParameters = parameters
        
    def insidePos(self,pos:Vector2):
        if detection_rect(self,pos+self.pivot.vector2()):
            dA = pos.x-self.pos.x+self.pivot.vector2().x

            self.value = self.valueMin + (self.valueMax-self.valueMin)* dA/self.size.x
            # self.functionParameters = self.value
            # print(" > new Value :",self.value)
            return True
        return False

    def setExtrmumSilder(self,max,min):
        self.valueMax = max
        self.valueMin = min

    def draw(self,layer:pygame.Surface):
        if self.visible:
            silderY = self.pos.y+self.size.y//2  -self.pivot.vector2().y
            draw_line(layer,Vector2(self.pos.x -self.pivot.vector2().x,silderY),Vector2(self.pos.x-self.pivot.vector2().x+self.size.x,silderY),self.color,self.thickness)
            silderX = self.pos.x + int((self.value-self.valueMin)/(self.valueMax-self.valueMin)*self.size.x) -self.pivot.vector2().x
            draw_cercle(layer,Vector2(silderX,silderY),int(self.size.y*0.42),self.color)

class Gui_Silder_Y(Gui_Silder):
    def __init__(self, pos: UDim2, size: UDim2, color, fillColor, thickness=4) -> None:
        super().__init__(pos, size, color, fillColor, thickness)
    def insidePos(self,pos:Vector2):
        if detection_rect(self,pos+self.pivot.vector2()):
            dA = pos.y-self.pos.y+self.pivot.vector2().y

            self.value = self.valueMin + (self.valueMax-self.valueMin)* dA/self.size.y
            # self.functionParameters = self.value
            # print(" > new Value :",self.value)
            return True
        return False
    def draw(self,layer:pygame.Surface):
        if self.visible:
            silderX = self.pos.x+self.size.x//2  -self.pivot.vector2().x
            draw_line(layer,Vector2(silderX,self.pos.y -self.pivot.vector2().y),Vector2(silderX,self.pos.y-self.pivot.vector2().y+self.size.y),self.color,self.thickness)
            silderY = self.pos.y + int((self.value-self.valueMin)/(self.valueMax-self.valueMin)*self.size.y) -self.pivot.vector2().y
            draw_cercle(layer,Vector2(silderX,silderY),int(self.size.x*0.42),self.color)

    

class Gui_Silder_WithText(Gui_Silder):
    def __init__(self, title:str, pos: UDim2, size: UDim2, color, fillColor, thickness=4) -> None:
        super().__init__(pos, size, color, fillColor, thickness)
        self.title:str = title 

    def draw(self,layer:pygame.Surface):
        if self.visible:
            draw_textComplexe(layer,self.title + str(self.value),self.pos.vector2()+self.size.vector2()*Vector2(0,0.35),Vector2(0,0.5),self.color,None,self.size.y//3)

            silderY = int(self.pos.y+self.size.y * 0.75) -self.pivot.vector2().y
            draw_line(layer,Vector2(self.pos.x-self.pivot.vector2().x,silderY),Vector2(self.pos.x-self.pivot.vector2().x+self.size.x,silderY),self.color,self.thickness)
            silderX = self.pos.x + int((self.value-self.valueMin)/(self.valueMax-self.valueMin)*self.size.x) -self.pivot.vector2().x
            draw_cercle(layer,Vector2(silderX,silderY),int(self.size.y*0.20),self.color)
            # draw_rect_empty(layer,self.pos.vector2(),self.size.vector2(),self.color)


# --- Render Functions --- #
def render():
    """Blit everything to the screen"""
    global __layers,__screen
    __screen.fill((255,255,255,255))

    if __renderAuto:
        for iLayer in __layers:
            surface = iLayer.surface_get_renderAuto()
            __screen.blit(surface, iLayer.pos)
    else:
        for iLayer in __layers:
            surface = iLayer.surface_get()
            __screen.blit(surface, iLayer.pos)
    pygame.display.flip()
    






# --- Events Function --- #

__table_touche = dict()
def __init_keyInput():
    #recuperation de la table qwerty
    """ Helps :
        les lettres ou chiffres : 'K_a', 'K_b', etc et 'K_1','K_2', etc
        les fleches : 'K_LEFT', 'K_RIGHT', 'K_DOWN', 'K_UP'
        la touche espace : 'K_SPACE'
        les touches control : 'K_LCTRL' (gauche) et 'K_RCTRL' (droit)
        les touches maj : 'K_LSHIFT' et 'K_RSHIFT'
        les touches alt : 'K_LALT' et 'K_RALT'
        la touche entrée : 'K_RETURN'
        la touche Retour arrière : 'K_BACKSPACE'
        le pavé numérique 'K_KP0', 'K_KP1', etc et 'K_KP_ENTER' (entree), 'K_KP_PLUS',
        'K_KP_MINUS', 'K_KP_MULTIPLY', 'K_KP_DIVIDE' (opérations) et 'K_KP_PERIOD' (point)
    """
    for val in dir(pygame):
        if val[0:2] == 'K_':
            __table_touche[val] = getattr(pygame,val)
    if __keybordType == "qwerty":
        __table_touche['K_a'] = pygame.K_q
        __table_touche['K_z'] = pygame.K_w
        __table_touche['K_q'] = pygame.K_a
        __table_touche['K_m'] = pygame.K_SEMICOLON
        __table_touche['K_w'] = pygame.K_z
        __table_touche['K_COMMA'] = pygame.K_m
        __table_touche['K_SEMICOLON'] = pygame.K_COMMA
__init_keyInput()

def changeKeybord_azerty():
    global __keybordType
    __keybordType = "azerty"
    __init_keyInput()

def changeKeybord_qwerty():
    global __keybordType
    __keybordType = "qwerty"
    __init_keyInput()

def keyPress(keyCode):
    return pygame.key.get_pressed()[__table_touche[keyCode]]

__linksId = 0
__linksInput_keyDown = list()
__linksInput_keyUp = list()
__linksInput_twoState = [(None,)]
__linksMouseInput_keyDown = list()
__linksMouseInput_keyUp = list()
__linksMouseInput_twoState = [(None,)]

def newLinkInput(keyName, functionToCall,parameters=None, idState=None):
    keyCode = __table_touche[keyName]
    return (keyName, keyCode, functionToCall,parameters,idState)

def addLinkInput_down(keyName, functionToCall,parameters=None):
    newLink = newLinkInput(keyName,functionToCall,parameters)
    __linksInput_keyDown.append(newLink)

def addLinkInput_up(keyName, functionToCall,parameters=None):
    newLink = newLinkInput(keyName,functionToCall,parameters)
    __linksInput_keyUp.append(newLink)
    
def addLinkInput_twoState(keyName, functionToCall,parameters=None):
    idState = len(__linksInput_twoState)

    newLink_down = newLinkInput(keyName, None,None,idState)
    __linksInput_keyDown.append(newLink_down)
    newLink_up   = newLinkInput(keyName, None,None,idState)
    __linksInput_keyUp.append(newLink_up)

    __linksInput_twoState.append([False,functionToCall,parameters])




""" Mouse """
def getMousePosition():
    """ return a tuple of two (x,y) """
    return Vector2(pygame.mouse.get_pos())

mouseTableDict = {"1":1,"2":2,"3":3,"left":1,"middle":2,"right":3}
def addLinkMouseInput_down(keyName, functionToCall,parameters=None):
    """ Info : left=1 , middle=2 , right=3 """
    # :Literal[1,2,3,"left","middle","right"]
    keyButton = mouseTableDict[str(keyName)]
    newLink = (keyButton, functionToCall,parameters,None)
    __linksMouseInput_keyDown.append(newLink)

def addLinkMouseInput_up(keyName, functionToCall,parameters=None):
    """ Info : left=1 , middle=2 , right=3 """
    # :Literal[1,2,3,"left","middle","right"]
    keyButton = mouseTableDict[str(keyName)]
    newLink = (keyButton, functionToCall,parameters,None)
    __linksMouseInput_keyUp.append(newLink)

def addLinkMouse_twoState(keyName, functionToCall,parameters=None):
    """ Info : left=1 , middle=2 , right=3 """
    # :Literal[1,2,3,"left","middle","right"]
    idState = len(__linksMouseInput_twoState)

    newLink_down = (keyName, None,None,idState)
    __linksMouseInput_keyDown.append(newLink_down)
    newLink_up   = (keyName, None,None,idState)
    __linksMouseInput_keyUp.append(newLink_up)

    __linksMouseInput_twoState.append([False,functionToCall,parameters])

__linksMouse_scroll = list()
def addLink_MouseScroll(functionToCall,parameters=None):
    """ Detecte the mouse whelle 
         > the function was call with function(mouseScroll,parameters=None)
         > mouseScroll = the number of cran """
    newLink = (functionToCall,parameters)
    __linksMouse_scroll.append(newLink)





# --- Cycle Function --- #
def wait(time):
    """ Time in millisecondes """
    pygame.time.wait(time)

def readListLinks (listLink,event,state=True):
    for iLink in listLink:
        if event.key == iLink[1]:
            if iLink[4]: # For input links repeate 
                __linksInput_twoState[iLink[4]][0] = state
            else:
                functionCall = iLink[2]
                if iLink[3] is not None :
                    functionCall(iLink[3])
                else:
                    functionCall()
            

def readListMouseLinks (listMouseLink,event,state=True):
    for iLink in listMouseLink:
        if event.button == iLink[0]:
            if iLink[3]: # For input links repeate 
                __linksMouseInput_twoState[iLink[3]][0] = state
            else:
                functionCall = iLink[1]
                if iLink[2] is not None :
                    functionCall(iLink[2])
                else:
                    functionCall()


def readListGuiButton():
    mousePosition = getMousePosition()
    for i, iLayer in enumerate(guiLayerList):
        resulte = iLayer.detectIfOneButtonClic(mousePosition)
        if resulte:
            return resulte


def oneCycle():
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if event.key == K_SPACE:
                print("Space Down")
            readListLinks(__linksInput_keyDown,event,True)

        elif event.type == KEYUP:
            if event.key == K_SPACE:
                print("Space Up")
            readListLinks(__linksInput_keyUp,event,False)

        elif event.type == MOUSEBUTTONDOWN:
            if not(readListGuiButton()):
                readListMouseLinks(__linksMouseInput_keyDown,event,True)

        elif event.type == MOUSEBUTTONUP:
            # if not(readListGuiButton()):
            readListMouseLinks(__linksMouseInput_keyUp,event,False)

        elif event.type == pygame.MOUSEWHEEL:
            # print(event.x, event.y)
            for iFunction in __linksMouse_scroll:
                if iFunction[1]: # test if parameters
                    iFunction[0](event.y,iFunction[2]) # call function
                else:
                    iFunction[0](event.y)

        elif event.type == pygame.VIDEORESIZE:
            # There's some code to add back window content here.
            print("New Resolution")
            setResolution((event.w, event.h))

        elif event.type == QUIT:
            pygame.quit()
            exit()


    for iInputRepeated in __linksInput_twoState:
        if iInputRepeated[0]:
            if iInputRepeated[2]: # test if parameters
                iInputRepeated[1](iInputRepeated[2]) # call function
            else:
                iInputRepeated[1]()
    for iInputRepeated in __linksMouseInput_twoState:
        if iInputRepeated[0]:
            if iInputRepeated[2]: # test if parameters
                iInputRepeated[1](iInputRepeated[2]) # call function
            else:
                iInputRepeated[1]()
    





# --- Chrono --- #

__CHRONOS = dict()
class Chrono:
    def __init__(self):
        self.debut = pygame.time.get_ticks()
        self.temps = 0
        self.en_cours = True

    def init(self):
        Chrono.__init__(self)

    def lancer(self):
        self.debut = pygame.time.get_ticks()
        self.en_cours = True

    def stop(self):
        self.temps += (pygame.time.get_ticks()-self.debut)
        self.en_cours = False

    def lire(self):
        if self.en_cours:
            return self.temps + (pygame.time.get_ticks()-self.debut)
        else:
            return self.temps

def init_chrono(chrono = "default"):
    """ remet le chronometre à 0 et le lance """
    if not(chrono in __CHRONOS):
        __CHRONOS[chrono] = Chrono()
    __CHRONOS[chrono].init()

def lance_chrono(chrono = "default"):
    """ lance le chronometre (sans le remettre a 0) """
    if not(chrono in __CHRONOS):
        __CHRONOS[chrono] = Chrono()
    __CHRONOS[chrono].lancer()

def stop_chrono(chrono = "default"):
    if not(chrono in __CHRONOS):
        __CHRONOS[chrono] = Chrono()
    __CHRONOS[chrono].stop()

def lire_chrono(chrono = "default"):
    if not(chrono in __CHRONOS):
        __CHRONOS[chrono] = Chrono()
    return __CHRONOS[chrono].lire()




# # Event loop
# continuer = True
# while continuer:
#     for event in pygame.event.get():
#         if event.type == QUIT:
#             continuer = False
#         elif event.type == KEYDOWN:
#             if event.key == K_SPACE:
#                 print("Space Down")
#             elif event.key == K_RETURN:
#                 print("Return Down")
#         elif event.type == KEYUP:
#             if event.key == K_SPACE:
#                 print("Space Up")


# pygame.quit()































