
"""
Club Info present : 
+------------------------------------------------------------------------------------+
| ██████╗░██╗░░░██╗███╗░░██╗███████╗░██████╗    ████████╗░█████╗░██████╗░██╗░█████╗░ |
| ██╔══██╗██║░░░██║████╗░██║██╔════╝██╔════╝    ╚══██╔══╝██╔══██╗██╔══██╗██║██╔══██╗ |
| ██████╔╝██║░░░██║██╔██╗██║█████╗░░╚█████╗░    ░░░██║░░░██║░░██║██████╔╝██║███████║ |
| ██╔══██╗██║░░░██║██║╚████║██╔══╝░░░╚═══██╗    ░░░██║░░░██║░░██║██╔═══╝░██║██╔══██║ |
| ██║░░██║╚██████╔╝██║░╚███║███████╗██████╔╝    ░░░██║░░░╚█████╔╝██║░░░░░██║██║░░██║ |
| ╚═╝░░╚═╝░╚═════╝░╚═╝░░╚══╝╚══════╝╚═════╝░    ░░░╚═╝░░░░╚════╝░╚═╝░░░░░╚═╝╚═╝░░╚═╝ |
+------------------------------------------------------------------------------------+

"""


# ----------- # Imports # ----------------- # 

# --- # Main Settings # --- #
from vector import Vector2
resolution = Vector2(640*1.5,480*1.5).floor()
__debugMode = True
__skipIntro = False
__badPerformance = False
__smallTerrain = True

# --- # Import Graphics # --- # 
import graphics2D

# --- # Load Graphics # --- #
graphics2D.setTitle("RunesTopia")
graphics2D.setResolution(resolution)


# --- # Import Loaded Screen # --- # 
import start_logo

# --- # Start Aniamtion # --- #
foregroundLayer = graphics2D.layer_newGuiLayer()
# if not(__debugMode):
start_logo.animation_start(foregroundLayer,14,__skipIntro)


# --- # Import data # --- #
start_logo.animation_nextLoad("Import/Data")
import data

# --- # Import screen render # --- #
start_logo.animation_nextLoad("Import/Display")
import display

# --- # Import Json Files # --- # 
start_logo.animation_nextLoad("read : Asset/Entity")
import Entity
start_logo.animation_nextLoad("read : Asset/Buildings")




# ----------- # In Start Game # ----------------- # 

# --- # Variables # --- #
__levelSize = Vector2(32,32)*3
__pathFindingGrid = None

__timePerFrame = 20

if __badPerformance or __smallTerrain:
    __smallTerrain = True
    __levelSize = Vector2(32,32)

if __badPerformance:
    __timePerFrame = 0

# --- # Creation New Game # --- #
start_logo.animation_nextLoad("Load : New Game")
MainLevel = data.game_newLevel(__levelSize)
start_logo.animation_nextLoad("Load : Terrain")
# data.loadNewTerrain(MainLevel)
MainLevel.load_heightMap_complexe(sizeCase=Vector2(2,2))

start_logo.animation_nextLoad("Assets/Sprties/Entity")
start_logo.animation_nextLoad("Assets/Sprties/Buildings")
start_logo.animation_nextLoad("Assets/Sprties/Terrains")
start_logo.animation_nextLoad("Assets/Sprties/Prefabs")


# --- # Creation Gui Menu # --- #
start_logo.animation_nextLoad("Assets/Gui")

# --- # Creation Terrain # --- #
start_logo.animation_nextLoad("Load Terrain")
display.render_terrain(MainLevel)




# ----------- # In Game # ----------------- # 

# --- # Load Inputs # --- #
start_logo.animation_nextLoad("Player Loading")

# --- # Inputs : Castle # --- # 
def placeCastle():
    mousePos = graphics2D.getMousePosition()
    global  __pathFindingGrid
    mousePos_tile = display.translate_pixelPos_to_tilePos(mousePos)
    if MainLevel.level_testIfIn(mousePos_tile):
        print(mousePos_tile)
        MainLevel.castle_setPosition(mousePos_tile)
    # __pathFindingGrid = data.pathFinding_grid_get(MainLevel,castlePosition)
        display.render_terrain(MainLevel,False)
    else:
        print("Error : Mouse clic out the terrain")
graphics2D.addLinkMouseInput_down(1,placeCastle)


# --- # Inputs : Entity # --- # 
def placeEntity():
    mousePos = graphics2D.getMousePosition()
    posTile = display.translate_pixelPos_to_tilePos(mousePos)
    MainLevel.entity_add(data.Entity(data.EntityType.zombie),posTile)
graphics2D.addLinkMouseInput_down(3,placeEntity)


# --- # Inputs : Buildings # --- # 
def place_structure(pos,newStructure):
    MainLevel.structure_set(pos,newStructure)
    display.render_terrain(MainLevel,False)
def place_wall():
    position = display.translate_pixelPos_to_tilePos(graphics2D.getMousePosition())
    place_structure(position,data.CaseType.wall)
graphics2D.addLinkInput_down("K_w",place_wall)
def place_path():
    position = display.translate_pixelPos_to_tilePos(graphics2D.getMousePosition())
    place_structure(position,data.CaseType.path)
graphics2D.addLinkInput_down("K_x",place_path)
def place_water():
    position = display.translate_pixelPos_to_tilePos(graphics2D.getMousePosition())
    place_structure(position,data.CaseType.water)
graphics2D.addLinkInput_down("K_c",place_water)



# --- # Inputs : Mouvements # --- # 
def camera_move_left():
    display.camera_pos_add(Vector2(0-1,0))
    display.render_cameraMoved()
def camera_move_right():
    display.camera_pos_add(Vector2(1,0))
    display.render_cameraMoved()
def camera_move_top():
    display.camera_pos_add(Vector2(0,0-1))
    display.render_cameraMoved()
def camera_move_down():
    display.camera_pos_add(Vector2(0,1))
    display.render_cameraMoved()
graphics2D.addLinkInput_twoState("K_z",camera_move_top)
graphics2D.addLinkInput_twoState("K_s",camera_move_down)
graphics2D.addLinkInput_twoState("K_d",camera_move_right)
graphics2D.addLinkInput_twoState("K_q",camera_move_left)
def mouseScroll(scrollY):
    display.camera_zoom_mouseScroll(scrollY)
    # render_terrain()
graphics2D.addLink_MouseScroll(mouseScroll)

graphics2D.addLinkMouse_twoState(2,display.mouseTranlation_press)
graphics2D.addLinkMouseInput_down(2,display.mouseTranlation_down)
graphics2D.addLinkMouseInput_up(2,display.mouseTranlation_up)

graphics2D.addLinkInput_down("K_p",mouseScroll, 1)
graphics2D.addLinkInput_down("K_m",mouseScroll,-1)



# --- # Other Custom Input # --- #
if __debugMode:
    def debugMode_changeTerrainRender(version):
        simpleVersion = version
        display.render_terrain(MainLevel,simpleVersion)
    def debugMode_NewTerrain(newMode=True):
        if newMode:
            MainLevel.load_heightMap_complexe()
        else:
            data.loadNewTerrain(MainLevel)
        display.render_terrain(MainLevel)

    graphics2D.addLinkInput_down("K_1",debugMode_changeTerrainRender,True)
    graphics2D.addLinkInput_down("K_2",debugMode_changeTerrainRender,False)
    graphics2D.addLinkInput_down("K_3",debugMode_NewTerrain,True)
    graphics2D.addLinkInput_down("K_4",debugMode_NewTerrain,False)


start_logo.animation_nextLoad(" ")
start_logo.animation_finish()
display.start()

# --- # Main Loop # --- #

__moveCard = None
__useCard = None

count_every5Frames = 0
while graphics2D.ifNotQuit():
    # --- # Test if New Wave # --- #
    # Spawn enemy
    pass

    # --- # Test if end Wave # --- # 
    # Give Rewards 
    pass

    # --- # Update Entity # --- # 
    count_every5Frames += 1
    if count_every5Frames >=15:
        MainLevel.entity_move()
    
    # --- # Update Buildings # --- # 
    pass
    
    # --- # Draw Entity # --- # 
    pass

    # --- # Draw Castle # --- # 
    pass

    # --- # Action : Move Cards # --- # 
    if __moveCard:
        # --- # Cards : Update Position # --- # 
        pass
        # --- # Cards : Re Draw Cards # --- # 
        pass

    # --- # Action : Place / Use Cards # --- # 
    if __useCard:
        # --- # Update Grid Animation # --- # 
        pass

        if "placeBuilding" == None:
            # --- # Data Change Tiles # --- # 
            pass
            # --- # Render Terrain # --- # 
            display.render_terrain(MainLevel)
        
        if "placeEntity" == None:
            # --- # Add New Entity # --- # 
            placeEntity()

        # --- # Update Counter Ressources # --- #
        display.update_counterRessources(MainLevel.getRessources_list())

    # --- # Game Info (top of gamebord) # --- #
    pass

    # --- # Update System of day/night # --- #
    pass
    
    # --- # New Screen Render # --- #
    display.render_cycle(MainLevel)
    # --- # Windows Update Screen # --- #
    graphics2D.render()
    # --- # Wait for the next frame # --- #

    graphics2D.wait(__timePerFrame)





