

import graphics2D
from graphics2D import UDim2
from vector import Vector2

import tilesGrid
tilesGrid.init()

from CaseType import CaseType


# ----- # Global Variable # ----- #
__resolution = graphics2D.getResolution()
tileSize = Vector2(16,16)

# ----- # Variable : World Position  # ----- #
__cameraPosition = Vector2(0,0)
__cameraZoom = 1
__cameraSpeed = 10

gamebord = Vector2(1000,1000)
tileSizePixel = tileSize*__cameraZoom

__terrainPositionLessBord = Vector2(50,50)
__terrainThicknessFrame = Vector2(12,14)
__terrainPosition = __terrainPositionLessBord + __terrainThicknessFrame
terrainSizePixel = Vector2(32,32)*tileSize
__terrainSizePixelBord = terrainSizePixel + __terrainThicknessFrame*2


# ----- # Variable : Colors  # ----- #
__colorLevel = ((54,99,101,255),(83,120,111,255),(102,147,68,255),(144,164,64,255),(171,184,70,255),(182,204,69,255))





# ----- # Variable : Gui Layer  # ----- #
layerGui = graphics2D.layer_newGuiLayer()
mainSurface_gameBoard = graphics2D.Gui_Frame_Scrollable(Vector2(0,0),UDim2(1,0,1,0),None,None,2)

temoiCenterO = graphics2D.Gui_Frame(Vector2(0,0),Vector2(15,15),(0,0,0,255),(255,255,255,255),5)
temoiCenterI = graphics2D.Gui_Frame(Vector2(20,0),Vector2(15,15),(0,0,0,255),(255,255,255,255),5)
temoiCenterJ = graphics2D.Gui_Frame(Vector2(0,20),Vector2(15,15),(0,0,0,255),(255,255,255,255),5)
temoiCenterII = graphics2D.Gui_Frame(Vector2(200,0),Vector2(15,15),(0,0,0,255),(255,255,255,255),5)
temoiCenterIII = graphics2D.Gui_Frame(Vector2(2000,0),Vector2(15,15),(0,0,0,255),(255,255,255,255),5)
mainSurface_gameBoard.objects_add(temoiCenterO)
mainSurface_gameBoard.objects_add(temoiCenterI)
mainSurface_gameBoard.objects_add(temoiCenterJ)
mainSurface_gameBoard.objects_add(temoiCenterII)
mainSurface_gameBoard.objects_add(temoiCenterIII)

# surface_terrain = graphics2D.Gui_Frame_Drawable(__terrainPositionLessBord,__terrainSizePixelBord,None,None,2,Vector2(0,0)) # (130,211,34,255)
surface_terrain = graphics2D.Gui_Frame_Drawable(__terrainPositionLessBord,__terrainSizePixelBord,None,None,2,Vector2(0,0)) # (130,211,34,255)
surface_entity  = graphics2D.Gui_Frame_Drawable(__terrainPositionLessBord,terrainSizePixel,None,None,2,Vector2(0,0)) # (0,0,0,255)
surface_cards   = graphics2D.Gui_Frame_Drawable(__terrainPosition+Vector2(20,__terrainSizePixelBord.y+20),Vector2(terrainSizePixel.x,16*6),(0,0,0,255),None,2,Vector2(0,0))

mainSurface_gameBoard.objects_add(surface_terrain)
mainSurface_gameBoard.objects_add(surface_entity )
mainSurface_gameBoard.objects_add(surface_cards  )
# mainSurface_gameBoard.objects_add(graphics2D.Gui_Frame(Vector2(0,0),gamebord,(0,0,0,255),None,2))

mainSurface_gameBoard.zoom_set(__cameraZoom)

# --- # Ressources Counter (at left) # --- #
surface_ressourcesCount:graphics2D.Gui_Frame = mainSurface_gameBoard.objects_add(graphics2D.Gui_Frame(Vector2(0,__terrainPositionLessBord.y),UDim2(0,180,0,250),(0,0,0,255),None,2,Vector2(1,0)))


# layerGui.objects_add(mainSurface_gameBoard) At the Start Index the main Frame 
mainSurface_gameBoard.backgroundImage_setup(graphics2D.image_load("ressources/templates/background.png"))



# ----- # Fonction : Camera Mouvement  # ----- #

def camera_pos_set(newPosition:Vector2):
    global __cameraPosition
    __cameraPosition = newPosition
    camera_pos_update()

def camera_pos_add(newDelta:Vector2):
    global __cameraPosition
    __cameraPosition += newDelta*__cameraSpeed #//__cameraZoom
    camera_pos_update()

def camera_pos_get():
    return __cameraPosition

def camera_pos_update():
    global __cameraPosition
    reso = graphics2D.getResolution()
    # if __cameraPosition.x<=0:
    #     __cameraPosition.x=0
    # if __cameraPosition.y<=0:
    #     __cameraPosition.y=0

    # if __cameraPosition.x>= gamebord.x-reso.x:
    #     __cameraPosition.x=gamebord.x-reso.x
    # if __cameraPosition.y>= gamebord.y-reso.y:
    #     __cameraPosition.y=gamebord.y-reso.y
    
    # if __cameraPosition.x>= gamebord.x-reso.x/__cameraZoom:
    #     __cameraPosition.x= gamebord.x-reso.x/__cameraZoom
    # if __cameraPosition.y>= gamebord.y-reso.y/__cameraZoom:
    #     __cameraPosition.y= gamebord.y-reso.y/__cameraZoom
    
    mainSurface_gameBoard.camera_position_set(__cameraPosition)
    render_cameraMoved()

def camera_zoom_mouseScroll(scrollY):
    global __cameraZoom,tileSizePixel,__cameraPosition
    mousePosition = graphics2D.getMousePosition()
    lastZoom = __cameraZoom
    print("New Zoom A :",__cameraZoom)
    if scrollY>0:
        __cameraZoom *= 2
    elif scrollY<0:
        __cameraZoom //= 2
    if __cameraZoom<0.25:   # Default 1
        __cameraZoom = 0.25
    elif __cameraZoom>8:
        __cameraZoom = 8
    print("New Zoom B :",__cameraZoom)


    tileSizePixel = (tileSize*__cameraZoom).floor()

    # layersUpdateZoom()
    mainSurface_gameBoard.zoom_set(__cameraZoom)
    if lastZoom!=__cameraZoom:
        if scrollY>0:
            """ Zoom """
            print("Zoom")
            # camera_pos_set(__cameraPosition - mousePosition/__cameraZoom )
            # camera_pos_set(__cameraPosition - (mousePosition)*lastZoom)
            # camera_pos_set(__cameraPosition - (mousePosition)*lastZoom) 
            # mainSurface_gameBoard.camera_position_set(mainSurface_gameBoard.pos_get()//2)
            
            # camera_pos_set(__cameraPosition*2 - (mousePosition))
            # camera_pos_set(__cameraPosition*2 - (mousePosition)//2)
            # camera_pos_set(__cameraPosition*2 - (mousePosition)*2)
            
            # camera_pos_set(__cameraPosition) # - (mousePosition*__cameraZoom))
            
            # camera_pos_set(__cameraPosition*2 + (mousePosition))
            # camera_pos_set((__cameraPosition+mousePosition*__cameraZoom)) Zoom : 0.5 -> 1
            # camera_pos_set((__cameraPosition+mousePosition*0.5))          Zoom : 1   -> 2
            # camera_pos_set((__cameraPosition+mousePosition*0.25))         Zoom : 2 -> 4
            camera_pos_set((__cameraPosition+mousePosition*(1/__cameraZoom)))
            pass
        else:
            print("DeZoom")
            """ Dezoom """
            # camera_pos_set(__cameraPosition + mousePosition/lastZoom )
            # camera_pos_set(__cameraPosition + (mousePosition)*__cameraZoom)
            # camera_pos_set(__cameraPosition + (mousePosition)*__cameraZoom)
            # mainSurface_gameBoard.camera_position_set(mainSurface_gameBoard.pos_get()*2)

            # camera_pos_set(__cameraPosition//2 + (mousePosition)/lastZoom*__cameraZoom)
            # camera_pos_set(__cameraPosition//2 + (mousePosition)//2)
            # camera_pos_set(__cameraPosition//2 + (mousePosition))
            
            # camera_pos_set((__cameraPosition+mousePosition)//2) # - (mousePosition*__cameraZoom))
            # camera_pos_set(__cameraPosition) # - (mousePosition*__cameraZoom))
            
            # camera_pos_set(__cameraPosition//2 - (mousePosition)/lastZoom*__cameraZoom)
            # camera_pos_set((__cameraPosition-(mousePosition)/2*__cameraZoom)) Zoom : 2 -> 1
            # camera_pos_set((__cameraPosition-(mousePosition)/4))              Zoom : 4 -> 2
            camera_pos_set((__cameraPosition-(mousePosition)/lastZoom))
            pass


    # camera_pos_set((__cameraPosition *lastZoom/__cameraZoom ).floor() )
    # camera_pos_set(((__cameraPosition-mousePosition) *__cameraZoom /lastZoom +mousePosition).floor() )
    render_cameraZoom()
    # print(__cameraPosition)

mousePosition_PressDown = None
def mouseTranlation_down():
    global mousePosition_PressDown
    mousePosition_PressDown = (graphics2D.getMousePosition(),camera_pos_get())
def mouseTranlation_press():
    global mousePosition_PressDown,__cameraZoom
    if mousePosition_PressDown:
        mousePos = graphics2D.getMousePosition()
        # camera_pos_set(mousePosition_PressDown[1] +(mousePos-mousePosition_PressDown[0])//__cameraZoom )
        # camera_pos_set(mousePosition_PressDown[1] +(mousePos-mousePosition_PressDown[0]))
        camera_pos_set(mousePosition_PressDown[1] - (mousePos-mousePosition_PressDown[0])//__cameraZoom)
        render_cameraMoved()
def mouseTranlation_up():
    global mousePosition_PressDown
    mousePosition_PressDown = None








# ----- # Fonction : Translation  # ----- #

def translate_tilePos_to_pixelPos(tilePos:Vector2):
    # return Vector2(tilePos.x,tilePos.y)*tileSizePixel + __terrainPosition + __cameraPosition*__cameraZoom
    return Vector2(tilePos.x,tilePos.y)*tileSizePixel//__cameraZoom + __terrainPosition - __cameraPosition

def translate_pixelPos_to_tilePos(pixelPos:Vector2):
    # return ((Vector2(pixelPos.x,pixelPos.y) -(__cameraPosition+ __terrainPosition)*__cameraZoom  )//tileSizePixel).floor()
    # return ((Vector2(pixelPos.x,pixelPos.y) -__cameraPosition -(__terrainPosition)*__cameraZoom  )//tileSizePixel).floor()
    return ((Vector2(pixelPos.x,pixelPos.y)/__cameraZoom +__cameraPosition -(__terrainPosition)  )*__cameraZoom//tileSizePixel).floor()

    (__cameraPosition+ mainSurface_gameBoard.pos_get().vector2() + mainSurface_gameBoard.size_get().vector2()*mainSurface_gameBoard.pivotPoint_get())
    reponce = ((Vector2(pixelPos.x,pixelPos.y) -(__cameraPosition+ mainSurface_gameBoard.pos_get().vector2() + mainSurface_gameBoard.size_get().vector2()*mainSurface_gameBoard.pivotPoint_get())*__cameraZoom  )//tileSizePixel).floor()
    print("reponce :",reponce)
    return reponce

def translate_tilePos_to_pixelPos_local(tilePos:Vector2):
    return __terrainThicknessFrame + Vector2(tilePos.x,tilePos.y)*tileSize

def translate_pixelPos_to_tilePos_local(pixelPos:Vector2):
    return (Vector2(pixelPos.x,pixelPos.y))//tileSize - __terrainThicknessFrame





# ----- # Fonction : Tiles Map  # ----- #

def render_tileMap_simple(gameLevel,pathFindingGrid):
    """ Render just Tile Map"""
    
    for iy,iLine in enumerate(gameLevel):
        for ix,case in enumerate(iLine):
            posPixel = translate_tilePos_to_pixelPos_local(Vector2(ix,iy))
            c = __colorLevel[case.level]
            graphics2D.draw_rect(surface_terrain,posPixel,tileSize,c)

            if case.type == CaseType.water:
                graphics2D.draw_rect_empty(surface_terrain,posPixel-1,tileSize+2,(0,202,225,255),5)
            if case.type == CaseType.tree:
                graphics2D.draw_rect_empty(surface_terrain,posPixel-1,tileSize+2,(0,255,64,255),2)

            if pathFindingGrid:
                direction = pathFindingGrid[iy][ix]
                if direction:
                    posCenter = posPixel +tileSize//2
                    graphics2D.draw_line(surface_terrain,posCenter,posCenter + direction*10, (0,0,0,255),2)
                    graphics2D.draw_cercle(surface_terrain,posCenter,4,(0,0,0,255))



def render_tileMap_getOneCase(gameLevel,gameSize:Vector2,pos:Vector2):
    if pos.x>=0 and pos.y>=0 and pos.x<gameSize.x and pos.y<gameSize.y:
        return gameLevel[pos.y][pos.x]
    return None

vectorDirectionNeighboud = (
    Vector2(-1,-1),Vector2(0,-1),Vector2(1,-1),
    Vector2(-1, 0),Vector2(0, 0),Vector2(1, 0),
    Vector2(-1, 1),Vector2(0, 1),Vector2(1, 1)
    )
def render_tileMap_getSituation(gameLevel,gameSize,pos:Vector2):
    situation = list()
    for iy in range(3):
        oneLine = list()
        for ix in range(3):
            addVector = vectorDirectionNeighboud[iy*3+ix]
            oneLine.append(render_tileMap_getOneCase(gameLevel,gameSize,pos + addVector))
        situation.append(oneLine)
    return situation
                                      


def render_tileMap(gameLevel,gameSize):
    """ Render just Tile Map"""
    tilesGrid.random_resetSeed()
    for iy,iLine in enumerate(gameLevel):
        for ix,case in enumerate(iLine):
            posPixel = translate_tilePos_to_pixelPos_local(Vector2(ix,iy))
            if case.type != 0:
                situation = render_tileMap_getSituation(gameLevel,gameSize,Vector2(ix,iy))
                tileGrass = tilesGrid.mainTemplate.template_level.getTiles(situation)

                # tileGrassB = tilesGrid.mainTemplate.getTilesComplexe(situation)
                tileGrassB = tilesGrid.mainTemplate.template_wall_wall.getTiles(situation)
                

                if tileGrass:
                    graphics2D.imageLoaded_paste_RGBA(surface_terrain,tileGrass,posPixel)  
                    if tileGrassB:
                        # print("Complexe Rule was Call :",tileGrassB)
                        if isinstance(tileGrassB, graphics2D.pygame.Surface):
                            graphics2D.imageLoaded_paste_RGBA(surface_terrain,tileGrassB,posPixel)    
                        else:
                            print("Error : AttributeError: 'pygame.Rect' object has no attribute 'convert_alpha'")
                    if case.type != 1:
                        tile = tilesGrid.mainTemplate.getTiles(situation)  
                        if tile:
                        # tile = graphics2D.image_resize(tile,tileSizePixel)
                            graphics2D.imageLoaded_paste_RGBA(surface_terrain,tile,posPixel)                     
                        else:
                            graphics2D.draw_rect_empty(surface_terrain,posPixel,tileSize,(100,100,100),1)

                else:       
                    c = __colorLevel[case.level]
                    graphics2D.draw_rect(surface_terrain,posPixel,tileSize,c)
            #graphics2D.drawn_rect(layerTerrain,posPixel,tileSize,c)
    


# --- # Draw Frame # --- #

def draw_mainFrame(layer,pos,size):
    c = [(229,177,129,255),(201,149,90,255),(102,63,57,255)]
    epaiseurA = 2
    epaiseurB = 8
    epaiseurC = 2
    graphics2D.draw_rect_empty(layer,pos+epaiseurA,size-epaiseurA,c[1],epaiseurB)
    graphics2D.draw_line(layer,pos+Vector2(epaiseurA,0),pos+size.OnlyX()-Vector2(epaiseurA,0),c[0],epaiseurA)
    graphics2D.draw_line(layer,pos+Vector2(0,epaiseurA),pos+size.OnlyY()-Vector2(0,epaiseurA),c[0],epaiseurA)
    graphics2D.draw_line(layer,pos+size.OnlyX()+Vector2(0,epaiseurA),pos+size-Vector2(0,epaiseurA),c[0],epaiseurA)
    graphics2D.draw_line(layer,pos+size.OnlyY()+Vector2(epaiseurA,0),pos+size-Vector2(epaiseurA,0),c[0],epaiseurA)
    
    graphics2D.draw_rect(layer,pos    +Vector2(epaiseurB+epaiseurA,epaiseurB+epaiseurA)  ,size.OnlyX()+Vector2((-epaiseurB-epaiseurA)*2,epaiseurC*2),c[2])
    graphics2D.draw_rect(layer,pos    +Vector2(epaiseurB+epaiseurA,epaiseurB+epaiseurA+1),size.OnlyY()+Vector2(epaiseurC,(-epaiseurB)*2-epaiseurA),c[2])
    graphics2D.draw_rect(layer,pos+size.OnlyX()+Vector2(-epaiseurB-epaiseurA,epaiseurB+epaiseurA),size.OnlyY()+Vector2(epaiseurC,(-epaiseurB)*2-epaiseurA),c[2])
    
    graphics2D.draw_line(layer,pos+Vector2(2,2),pos+Vector2(4,4),c[0],1)
    graphics2D.draw_line(layer,pos+size-Vector2(2,2),pos+size-Vector2(4,4),c[0],1)
    graphics2D.draw_line(layer,pos+size.OnlyX()+Vector2(-2,2),pos+size.OnlyX()+Vector2(-4,4),c[0],1)
    graphics2D.draw_line(layer,pos+size.OnlyY()+Vector2(2,-2),pos+size.OnlyY()+Vector2(4,-4),c[0],1)


# --- # Cards Gestions # --- #

# layerCards_surface = graphics2D.Layer(Vector2(300,500))
image_cardEmpty = graphics2D.image_load_RGBA("ressources/templates/card.png")
def render_layer_card(listCard:list):
    # global layerCards_surface
    deltaCard = 8
    sizeCard = Vector2(image_cardEmpty.get_size())
    surface_cards.surface_new()
    # layerCards_surface = graphics2D.Layer(Vector2(len(listCard)*(sizeCard[0]+deltaCard),sizeCard[1])*__cameraZoom)
    # surface_cards.surface_clear()
    # image_cardEmpty_copy = graphics2D.image_resize(image_cardEmpty.copy(),sizeCard*__cameraZoom)
    # for i, iCard in enumerate(listCard):
    #     graphics2D.imageLoaded_paste_RGBA(layerCards_surface,image_cardEmpty_copy,Vector2((sizeCard[0]+deltaCard)*__cameraZoom*i,0))
    for i, iCard in enumerate(listCard):
        graphics2D.imageLoaded_paste_RGBA(surface_cards,image_cardEmpty,Vector2((sizeCard[0]+deltaCard)*i,0))
    surface_cards.surface_render()



# --- # Custom Buildings # --- #

def draw_Castle(castlePos:Vector2):
    castleImage = graphics2D.image_clip(tilesGrid.mainTemplate.image, Vector2(0,12)*16, Vector2(16,16)*3)
    posPixel = translate_tilePos_to_pixelPos_local(castlePos)
    graphics2D.draw_rect_empty(surface_terrain,posPixel+1,tileSize-2,(120,120,120,255),4)
    graphics2D.draw_rect_empty(surface_terrain,posPixel+2,tileSize-4,(180,180,180,255),2)
    graphics2D.imageLoaded_paste_RGBA(surface_terrain,castleImage,posPixel-Vector2(16,16))



# --- # Entity Gestion # --- #
enemy_render = graphics2D.image_load_RGBA("ressources/templates/goblin.png")
def draw_entity(gameLevel,entityList:list):
    """ entityList = list[Vector2] """
    surface_entity.surface_new()
    demiTile = tileSize//2
    for entityPos in entityList:
        caseData = gameLevel[entityPos.y][entityPos.x]
        nbrEntity = caseData.entity_number()
        if nbrEntity > 0:
            entityList = caseData.entity_get()
            casePosPixel = translate_tilePos_to_pixelPos_local(entityPos)
            # graphics2D.drawn_cercle(layerEntity,casePosPixel + demiTile,8,(0,0,0,255))
            graphics2D.imageLoaded_paste_RGBA(surface_entity,enemy_render,casePosPixel + demiTile-Vector2(16,16))


# --- # Counter Ressources # --- #
def created_counterRessources():
    ressourcesNameList = ("Food","Gold","Wood","Stone","Potion","Runes")
    ressourcesNumbList = (100,50,2000,20,1,5)
    ressourcesImageList = (Vector2(0,3),Vector2(2,2),Vector2(1,0),Vector2(1,0),Vector2(0,5),Vector2(1,4))

    imageItem = graphics2D.image_load_RGBA("ressources/templates/template_items.png")
    

    surface_ressourcesCount.UIListLayout_setup(10)

    for i in range(len(ressourcesNameList)):
        newCounter = graphics2D.Gui_Frame(Vector2(0,0),UDim2(1,0,0,30),(0,0,0,255),None,2)
        surface_ressourcesCount.objects_add(newCounter)

        imageLoaded = graphics2D.image_clip(imageItem,ressourcesImageList[i]*16,Vector2(16,16))

        textLabel  = graphics2D.Gui_TextLabel(    ressourcesNameList[i] ,UDim2(0,4 ,0.5,0),None,(250,250,250,255),None,16,2,Vector2(0,0.5),None,Vector2(0,0.5))
        textNumber = graphics2D.Gui_TextLabel(str(ressourcesNumbList[i]),UDim2(1,-40,0.5,0),None,(250,250,250,255),None,16,2,Vector2(1,0.5),None,Vector2(1,0.5))
        imageLabel = graphics2D.Gui_Image(imageLoaded,UDim2(1,-4,0.5,0),UDim2(0,30,1,0),None,(255,255,255,255),0,Vector2(1,0.5))
        newCounter.objects_add(textLabel)
        newCounter.objects_add(textNumber)
        newCounter.objects_add(imageLabel)

        newCounter.surface_render_all()
    
    imageItem= None
    surface_ressourcesCount.UIListLayout_order()

def update_counterRessources(newRessourcesCount):
    """ ("Food","Gold","Wood","Stone","Potion","Runes") """
    for i, newRessource in enumerate(newRessourcesCount) :
        surface_ressourcesCount.objects[i].objects[1].text_set(str(newRessource))


# ----- # Render Function # ----- #


def render_terrain(game, simple=False):
    """ Render the map of tiles"""

    print("Render Terrain")

    gameLevel = game.levelMap_get()
    gameSize = game.size_get()
    castlePos = game.castlePos
    pathFindingGrid = game.pathFindingToCastle

    terrainSizePixel.x = gameSize.x * tileSize.x
    terrainSizePixel.y = gameSize.y * tileSize.y

    global __terrainSizePixelBord
    __terrainSizePixelBord = terrainSizePixel + __terrainThicknessFrame*2

    surface_terrain.size_set(__terrainSizePixelBord)

    surface_terrain.surface_new()
    draw_mainFrame(surface_terrain,Vector2(0,0),__terrainSizePixelBord-Vector2(2,6))
    if simple:
        render_tileMap_simple(gameLevel,pathFindingGrid)
    else:
        render_tileMap(gameLevel,gameSize)

    # draw_mainFrame()
    draw_Castle(castlePos)

    render_layer_card([0,1,2,3,4])
    
    # mainSurface_gameBoard.surface_render()


def render_cameraZoom():
    # print("Render Camera Zoom")
    # surface_terrain.surface_render()
    render_cameraMoved()


def render_cameraMoved():
    # print("Render Camera Moved")
    # mainSurface_gameBoard.surface_render()
    pass


# --- # Load # --- #
def start():
    layerGui.objects_add(mainSurface_gameBoard)
    
    created_counterRessources()

    render_layer_card([0,1,2,3,4])
    layerGui.surface_render_all()


def render_cycle(game):
    # global x
    # x += 1
    # print(x)

    gameLevel = game.levelMap_get()

    # surface_entity.surface.fill((0,0,0,0))
    draw_entity(gameLevel,game.entity_get())

    # mainSurface_gameBoard.surface_clear()


    # layerGui.surface_render_all()
    # graphics2D.render()

    # layerGui.surface_render()
    # layerGui.surface_render_all()
    # mainSurface_gameBoard.surface_render()
    # layerGui.surface_render()


