
# import cv2
# import matplotlib.pyplot as plt
import graphics2D
# from PIL import Image
from vector import Vector2

import tilesRules
from CaseType import CaseType



mainTemplate = None
sizeTile = Vector2(16,16)


def getRegion_openCV(img,start:Vector2,end:Vector2):
    return img[start.x:end.x, start.y:end.y]

def cutTemplate(img):
    # imageShape = img.shape  # For cv2
    imageShape = graphics2D.image_getSize(img)
    sizeImage = Vector2(imageShape[0],imageShape[1])

    tilesList = list()
    for iy in range(sizeImage.y//sizeTile.y):
        tilesLineList = list()
        for ix in range(sizeImage.x//sizeTile.x): 
            posPixel = Vector2(ix*sizeTile.x,iy*sizeTile.y)
            # partImage = getRegion_openCV(img,posPixel, posPixel+sizeTile) # For cv2
            partImage = graphics2D.image_clip(img,posPixel, sizeTile)
            tilesLineList.append(partImage )
            # print(" M : X:",ix,len(tilesLineList))
            # print(" M : Y:",iy,len(tilesList))
            
        tilesList.append(tilesLineList)
        # print(" M : X:",ix,len(tilesList[0]))
        # print(" M : Y:",iy,len(tilesList))

    return tilesList







class MiniTemplate:
    def __init__(self) -> None:
        pass



class MiniTemplate_zone(MiniTemplate):
    def __init__(self,ruleFunction,tiles,soloId = 11) -> None:
        super().__init__()

        if soloId:
            if isinstance(soloId,int):
                self.solo = (tiles[soloId],)
            elif (isinstance(soloId,tuple) or isinstance(soloId,list)) and isinstance(soloId[0],int):
                self.solo = tuple()
                for i in soloId:
                    self.solo += (tiles[i],)
            else:
                print("  > ",soloId)
                self.solo = soloId

        self.center = tiles[4]
        self.left = tiles[3]
        self.right = tiles[5]
        self.top = tiles[7]
        self.bottom = tiles[1]
        self.corner_TopLeft = tiles[6]
        self.corner_TopRight = tiles[8]
        self.corner_BottomLeft = tiles[0]
        self.corner_BottomRight = tiles[2]
        self.cornerInvers_TopLeft = tiles[10]
        self.cornerInvers_TopRight = tiles[9]
        self.cornerInvers_BottomLeft = tiles[13]
        self.cornerInvers_BottomRight = tiles[12]

        self.rules = ruleFunction(self)

    def getTiles(self,situation):
        return tilesRules.getGoodTile(self.rules,situation)
    
class MiniTemplate_path(MiniTemplate):
    def __init__(self,ruleFunction,tiles) -> None:
        """ 0 to 15       
            ▮▮▮ ▮▮▮ ▮▮▮ ▮▮▮ 
            ▮▯▮ ▮▯▯ ▯▯▯ ▯▯▮ 
            ▮▮▮ ▮▮▮ ▮▮▮ ▮▮▮ 

            ▮▮▮ ▮▮▮ ▮▮▮ ▮▮▮ 
            ▮▯▮ ▮▯▯ ▯▯▯ ▯▯▮ 
            ▮▯▮ ▮▯▮ ▮▯▮ ▮▯▮ 

            ▮▯▮ ▮▯▮ ▮▯▮ ▮▯▮ 
            ▮▯▮ ▮▯▯ ▯▯▯ ▯▯▮ 
            ▮▯▮ ▮▯▮ ▮▯▮ ▮▯▮ 

            ▮▯▮ ▮▯▮ ▮▯▮ ▮▯▮ 
            ▮▯▮ ▮▯▯ ▯▯▯ ▯▯▮ 
            ▮▮▮ ▮▮▮ ▮▮▮ ▮▮▮  """
        
        super().__init__()

        self.solo = tiles[0]
        self.center = tiles[10]

        self.left = tiles[3]
        self.right = tiles[1]
        self.top = tiles[4]
        self.bottom = tiles[12]

        self.x = tiles[2]
        self.y = tiles[8]

        self.corner_TopLeft = tiles[13]
        self.corner_TopRight = tiles[15]
        self.corner_BottomLeft = tiles[5]
        self.corner_BottomRight = tiles[7]

        self.croisement_Left = tiles[9]
        self.croisement_Right = tiles[11]
        self.croisement_Top = tiles[14]
        self.croisement_Bottom = tiles[6]

        self.rules = ruleFunction(self)

    def getTiles(self,situation):
        return tilesRules.getGoodTile(self.rules,situation)
    


class MiniTemplate_dynamique(MiniTemplate):
    def __init__(self,tiles, caseType) -> None:
        """ 0 to 8                   9 to 17
            ▯▯▯ ▯▯▯ ▯▯▯       ▮▮▮ ▮▮▮ ▮▮▮
            ▯▯▯ ▯▯▯ ▯▯▯       ▮▯▯ ▯▯▮ ▮▯▮
            ▯▯▮ ▮▮▮ ▮▯▯       ▮▮▮ ▯▯▮ ▮▯▮

            ▯▯▮ ▮▮▮ ▮▯▯       ▮▮▮ ▯▯▯ ▯▯▮
            ▯▯▮ ▮▯▮ ▮▯▯       ▮▯▯ ▯▯▯ ▯▯▮
            ▯▯▮ ▮▮▮ ▮▯▯       ▮▯▯ ▯▯▯ ▮▮▮

            ▯▯▮ ▮▮▮ ▮▯▯       ▮▯▮ ▮▯▯ ▮▮▮
            ▯▯▯ ▯▯▯ ▯▯▯       ▮▯▮ ▮▯▯ ▯▯▮
            ▯▯▯ ▯▯▯ ▯▯▯       ▮▮▮ ▮▮▮ ▮▮▮ """
        
        super().__init__()

        self.solo = tiles[13]
        self.center = tiles[4]
        
        self.corner_TopLeft     = tiles[8]
        self.corner_TopRight    = tiles[6]
        self.corner_BottomLeft  = tiles[2]
        self.corner_BottomRight = tiles[0]

        self.left   = tiles[5]
        self.right  = tiles[3]
        self.top    = tiles[7]
        self.bottom = tiles[1]
        
        self.cornerInvers_TopLeft     = tiles[12]
        self.cornerInvers_TopRight    = tiles[10]
        self.cornerInvers_BottomLeft  = tiles[16]
        self.cornerInvers_BottomRight = tiles[14]
        
        self.impasse_Left   = tiles[9]
        self.impasse_Right  = tiles[17]
        self.impasse_Top    = tiles[11]
        self.impasse_Bottom = tiles[15]

        self.x = graphics2D.image_concatenation(self.top,self.bottom)
        self.y = graphics2D.image_concatenation(self.left,self.right)

        self.canConnectTo = caseType

        """ 
            left,        bottomn,     right,                    top
              0     1      2     3      4     5      6    7       8     9     10     11     12    13     14    15
            ▯▯▯ ▮▯▯ ▯▯▯ ▮▯▯  ▯▯▮ ▮▯▮ ▯▯▮ ▮▯▮  ▮▮▮ ▮▮▮ ▮▮▮ ▮▮▮  ▮▮▮ ▮▮▮ ▮▮▮ ▮▮▮
            ▯▯▯ ▮▯▯ ▯▯▯ ▮▯▯  ▯▯▮ ▮▯▮ ▯▯▮ ▮▯▮  ▯▯▯ ▮▯▯ ▯▯▯ ▮▯▯  ▯▯▮ ▮▯▮ ▯▯▮ ▮▯▮
            ▯▯▯ ▮▯▯ ▮▮▮ ▮▮▮  ▯▯▮ ▮▯▮ ▮▮▮ ▮▮▮  ▯▯▯ ▮▯▯ ▮▮▮ ▮▮▮  ▯▯▮ ▮▯▮ ▮▮▮ ▮▮▮
                         .            ..           .             ...         .             ..          .
            if accept corner (topLeft, topRight, bottomLeft, bottomRight)
            ▮▮   ▯▮   ▮▮   ▯▮   ▮▯   ▯▯   ▮▯   ▯▯   ▯▯   ▯▯   ▯▯   ▯▯   ▯▯   ▯▯   ▯▯   ▯▯   
            ▮▮   ▯▮   ▯▯   ▯▯   ▮▯   ▯▯   ▯▯   ▯▯   ▮▮   ▯▮   ▯▯   ▯▯   ▮▯   ▯▯   ▯▯   ▯▯   
        """
        self.tiles = [
            # Tile / By number           # (topLeft, topRight, bottomLeft, bottomRight)
            (self.solo,                     True , True , True , True  ),
            (self.left,                     False, True , False, True  ),
            (self.bottom,                   True , True , False, False ),
            (self.cornerInvers_BottomLeft,  False, True , False, False ),
            
            (self.right,                    True , False, True , False ),
            (self.y,                        False, False, False, False ),
            (self.cornerInvers_BottomRight, True , False, False, False ),
            (self.impasse_Bottom,           False, False, False, False ),
            
            (self.top,                      False, False, True , True  ),
            (self.cornerInvers_TopLeft,     False, False, False, True  ),
            (self.x,                        False, False, False, False ),
            (self.impasse_Left,             False, False, False, False ),

            (self.cornerInvers_TopRight,    False, False, True , False ),
            (self.impasse_Top,              False, False, False, False ),
            (self.impasse_Right,            False, False, False, False ),
            (self.center,                   False, False, False, False )
        ]
        
    def translateSituation(self,situation):
        newSituation = []
        goodCase = 0
        centerLevel = situation[1][1].level
        # print([[ix.type for ix in iy] for iy in situation])
        for iLine in situation:
            newLine = []
            for iCase in iLine:
                test = (iCase!=None and self.canConnectTo == iCase.type and iCase.level >= centerLevel)
                # print(" >",iCase.type,":",test)
                goodCase += int(test)
                newLine.append(test)
            newSituation.append(newLine)
        # print(newSituation)
        return (newSituation,goodCase)


    def getTiles(self,situationGived):
        situation, neighbour = self.translateSituation(situationGived)
        if neighbour == 0:
            # print(" /!\ No neighbour ! ")
            return None
        
        if situation[1][1]:
            # print(" /!\ Always Wall here ! ")
            return None

        # Translate binaire to int
        #         Left                   Bottom                   Right                    Top
        tile_id = int(situation[1][0])*1 + int(situation[2][1])*2 + int(situation[1][2])*4 + int(situation[0][1])*8
        # tile_id = int(situation[1][0]) + int(situation[2][1])*2 + int(situation[1][2])*4 + int(situation[0][1])*8
        
        tileReference = self.tiles[tile_id]
        tile = tileReference[0].copy()

        # Past Coin if possible 
        (graphics2D.imageLoaded_pasteTo_imageLoadedRGBA(tile, self.corner_TopLeft    ,Vector2(0,0))) if (situation[0][0] and tileReference[1]) else tile
        (graphics2D.imageLoaded_pasteTo_imageLoadedRGBA(tile, self.corner_TopRight   ,Vector2(0,0))) if (situation[0][2] and tileReference[2]) else tile
        (graphics2D.imageLoaded_pasteTo_imageLoadedRGBA(tile, self.corner_BottomLeft ,Vector2(0,0))) if (situation[2][0] and tileReference[3]) else tile
        (graphics2D.imageLoaded_pasteTo_imageLoadedRGBA(tile, self.corner_BottomRight,Vector2(0,0))) if (situation[2][2] and tileReference[4]) else tile

        # Info De Debug
        # print("neighbour :",neighbour)
        # print("tile_id :",tile_id)
        # print("tileReference :",tileReference)
        # print("tile :",tile)

        return tile



class Template:
    def __init__(self,imageName:str) -> None:
        self.imageName = imageName
        # self.image = cv2.imread(imageName,-1)
        self.image = graphics2D.image_load_RGBA(imageName)
        self.tiles = list()
        
        self.tiles = cutTemplate(self.image)
        print("X:",len(self.tiles[0]))
        print("Y:",len(self.tiles))

    def getListTiles(self,start:Vector2,end:Vector2):
        partList = list()
        print("X:",len(self.tiles[0]))
        print("Y:",len(self.tiles))
        for iLine in range(start.y,end.y+1):
            # partList += self.tiles[iLine][start.x:(end.x+1)]
            for iColum in range(start.x,end.x+1):
                # print("> ",iColum,iLine)
                partList.append(self.tiles[iLine][iColum])
        # for iLine in range(start.x,end.x+1):
        #     print("> ",len(self.tiles[iLine]))
        #     partList += self.tiles[iLine][start.y:(end.y+1)]
        return partList
    
    def loadTemplate(self):
        self.template_level = MiniTemplate_zone(tilesRules.newRulesList_level_out,self.getListTiles(Vector2(6,4),Vector2( 8,8)) , self.getListTiles(Vector2(0,0),Vector2(2,3)))
        self.template_foret = MiniTemplate_zone(tilesRules.newRulesList_zone ,self.getListTiles(Vector2(9,4),Vector2(11,8)))
        self.template_water = MiniTemplate_zone(tilesRules.newRulesList_zone ,self.getListTiles(Vector2(0,4),Vector2(2,8)), [4])
        self.template_path  = MiniTemplate_path(tilesRules.newRulesList_path ,self.getListTiles(Vector2(4,0),Vector2(7,4)))
        
        self.template_wall = MiniTemplate_path(tilesRules.newRulesList_path ,self.getListTiles(Vector2(8,12),Vector2(11,15)))
        self.template_wall_wall = MiniTemplate_dynamique(self.getListTiles(Vector2(3,15),Vector2(5,20)),CaseType.wall)
        # self.template_path2 = MiniTemplate_path(tilesRules.newRulesList_path ,self.getListTiles(Vector2(8,0),Vector2(11,3)))
        # self.rules = tilesRules.newRulesList(self)

        self.complexeRules = tilesRules.newRulesList_complexe(self.tiles)

    def getTiles(self,situation):
        caseData = situation[1][1]
        caseType = caseData.type
        return (self.template_level,self.template_water,self.template_foret,self.template_path,self.template_wall)[caseType-1].getTiles(situation)
        # return tilesRules.getGoodTile(self.rules,situation)
    
    def getTilesComplexe(self,situation):
        return tilesRules.getGoodTile(self.complexeRules,situation)


def random_resetSeed():
    tilesRules.random_resetSeed()


def init():
    global mainTemplate
    mainTemplate = Template("ressources/templates/template_foret.png")
    mainTemplate.loadTemplate()





if __name__ == "__main__":
    print(" --- Local Test : Lib Tile Grid --- ")
    graphics2D.init((128,128)," -- Test -- ")
    init()
    from data import Case  # To Simulmate Case's Situations
    from CaseType import CaseType

    situation = [[Case() for ix in range(3)] for iy in range(3)]
    mi = [
        [True,None,True],
        [None,None,True],
        [True,None,True]
    ]
    [[(situation[iy][ix].setType(CaseType.wall)) if mi[iy][ix] else None for ix in range(3)] for iy in range(3)]
    print(" Situation ")
    for iy in range(3):
        s = " | "
        for ix in range(3):
            if mi[iy][ix]:
                s += "▮ "
            else:
                s += "▯ "
        print(s+ " |")
    tileGrassB = mainTemplate.template_wall_wall.getTiles(situation)
    print("Tile Finale :",tileGrassB)
    
    if tileGrassB:
        layer = graphics2D.layer_new()
        graphics2D.imageLoaded_paste_RGBA(layer,graphics2D.image_resize(tileGrassB,Vector2(128,128)),Vector2(0,0))
        graphics2D.render()
        graphics2D.waitQuit()
    
