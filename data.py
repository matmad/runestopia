
from vector import Vector2

from CaseType import CaseType, InfoCases
from Entity import *

import NoiseLib


class Case:
    def __init__(self):
        self.level = 1
        self.type = CaseType.grass

        self.entity = list()

    def setLevel(self,newLevel):
        self.level = newLevel
        
    def setType(self,newType):
        self.type = newType

    # ----- # Entity # ----- #
    def entity_add(self,newEntity:Entity):
        self.entity.append(newEntity)
    def entity_get(self):
        return self.entity
    def entity_number(self) -> int:
        return self.entity.__len__()
    def entity_remove(self):
        self.entity = list()
    def entity_set(self,newList):
        self.entity = newList
        
    # ----- # Structures # ----- #
    def structure_set(self, newStucture):
        self.type = newStucture


def game_newMap_emptyLevel(size:Vector2) -> list:
    """ Return list[list[Case]] = map[Case] """
    return [[Case() for ix in range(size.x)] for iy in range(size.y)]


class GameLevel:
    def __init__(self,size:Vector2):
        self.map = game_newMap_emptyLevel(size)
        self.size = size

        self.entity = list()
        self.pathFindingToCastle = None

        self.ressources = {
            "Food":0,
            "Gold":0,
            "Wood":0,
            "Stone":0,
            "Potion":0,
            "Runes":0
        }

        self.castlePos = Vector2(1,1)


    def getRessources(self) -> dict:
        """ Return Dict """
        """ ("Food","Gold","Wood","Stone","Potion","Runes") """
        return self.ressources
    def getRessources_list(self) -> list:
        """ Return List """
        """ ("Food","Gold","Wood","Stone","Potion","Runes") """
        return [self.ressources[i] for i in ("Food","Gold","Wood","Stone","Potion","Runes") ]

    # --- # Terrain Gestion # --- #
    def terrain_reset(self):
        self.map = game_newMap_emptyLevel(self.size)

    def load_heightMap_simple(self,heightMap,sizeCase=Vector2(1,1)):
        s = self.size
        for iy in range(s.y//sizeCase.y):
            for ix in range(s.x//sizeCase.x):
                for jy in range(sizeCase.y):
                    for jx in range(sizeCase.x):
                        newLevel = heightMap[iy][ix]
                        case:Case = self.map[iy*sizeCase.y+jy][ix*sizeCase.x+jx]
                        if newLevel == 0:
                            case.setType(CaseType.water)
                            case.setLevel(1)
                        elif newLevel == 1:
                            case.setType(CaseType.grass)
                            case.setLevel(1)
                        elif newLevel == 2:
                            case.setType(CaseType.tree)
                            case.setLevel(1)
                        elif newLevel >= 3:
                            case.setType(CaseType.grass)
                            case.setLevel(newLevel-1)

    def load_heightMap_complexe(self,sizeCase=Vector2(2,2)):
        s = self.size
        self.terrain_reset()

        # --- # Terrain # --- #
        noiseTerrain = NoiseLib.NoiseMap(s//2)
        noiseTerrain.new()
        noiseTerrain.floorToLevel(8)
        # hightMap.test_color()
        print("New Level :")
        noiseTerrain.test_print_color_mini()
        
        # --- # Tree # --- #
        noiseTree = NoiseLib.NoiseMap(s//2)
        noiseTree.new()
        noiseTree.floorToLevel(2)
        noiseTree.test_print_color_mini()

        for iy in range(s.y//sizeCase.y):
            for ix in range(s.x//sizeCase.x):
                for jy in range(sizeCase.y):
                    for jx in range(sizeCase.x):
                        newLevel = noiseTerrain.noise[iy][ix]
                        case:Case = self.map[iy*sizeCase.y+jy][ix*sizeCase.x+jx]
                        if newLevel <=1:
                            case.setType(CaseType.water)
                            case.setLevel(1)
                        else:
                            case.setLevel((newLevel)//2 )
                            ifTree = noiseTree.noise[iy][ix] == 0
                            if newLevel % 2 == 1 and ifTree: 
                                case.setType(CaseType.tree)
                                case.setLevel((newLevel)//2)

        # # --- # Tree # --- #
        # noiseTree = NoiseLib.NoiseMap(s//2)
        # noiseTree.new()
        # noiseTree.floorToLevel(3)
        # # hightMap.test_color()
        # print("New Level :")
        # noiseTree.test_print_color()
        # for iy in range(s.y//sizeCase.y):
        #     for ix in range(s.x//sizeCase.x):
        #         for jy in range(sizeCase.y):
        #             for jx in range(sizeCase.x):
        #                 newLevel = noiseTree.noise[iy][ix]
        #                 case:Case = self.map[iy*sizeCase.y+jy][ix*sizeCase.x+jx]
        #                 if newLevel ==0:
        #                     case.setType(CaseType.tree)
    
    def castle_setPosition(self,newCastlePos:Vector2):
        self.castlePos = newCastlePos
        print("New Castle Pos :",newCastlePos)
        self.pathFinding_new()


    # ----- # Path Finding # ----- #
    def pathFinding_new(self):
        if self.castlePos.x>=0 and self.castlePos.y>=0:
            self.pathFindingToCastle = pathFinding_grid_get(self,self.castlePos)
        else:
            print("Error : Emplacement of castle with negatif coordonates")



    # ----- # Level # ----- #
    def levelMap_get(self):
        return self.map
    def levelMap_get_str(self):
        s = ""
        for iLine in self.map:
            s += " | "
            for iCase in iLine:
                v = str(iCase.level)
                s += v + " "*(2-len(v))
            s += "|\n"
        return s
    def levelMap_set(self, newMap):
        self.map = newMap

    def level_testIfIn(self,pos:Vector2):
        return pos.x>=0 and pos.y>=0 and pos.x<self.size.x and pos.y<self.size.y


    def size_get(self):
        return self.size
    


    def case_get(self,pos:Vector2) -> Case:
        if self.level_testIfIn(pos):
            return self.map[pos.y][pos.x]


    # ----- # Entity # ----- #
    def entity_add(self,newEntity:Entity,pos:Vector2):
        case = self.case_get(pos)
        if case:
            case.entity_add(pos)
            self.entity.append(pos)
    def entity_get(self) -> list:
        """ Return list[Vector2] """
        return self.entity
    def entity_number(self) -> int:
        return self.entity.__len__()
    

    def entity_move(self):
        if self.pathFindingToCastle:
            newListPos = list()
            newListEntity = list()
            for iEntiyPos in self.entity_get():
                entity = self.map[iEntiyPos.y][iEntiyPos.x].entity_get()
                velocity = self.pathFindingToCastle[iEntiyPos.y][iEntiyPos.x]
                if velocity is None:
                    velocity = Vector2(0,0)

                newPositionTile = iEntiyPos+ velocity
                if not(newPositionTile == self.castlePos):
                    newListEntity.append(entity)
                    newListPos.append(newPositionTile)

                    self.map[iEntiyPos.y][iEntiyPos.x].entity_remove()
            for iEntiyPos in newListPos:
                self.map[iEntiyPos.y][iEntiyPos.x].entity_set(newListEntity)
            self.entity = newListPos

    # ----- # Structures # ----- #

    def structure_set(self,pos,structureType):
        case = self.case_get(pos)
        if case:
            case.structure_set(structureType)
            



def game_newLevel(size):
    newLevel = GameLevel(size)
    return newLevel




def pathFinding_grid_addItemToList(listCaseToUpdate:list,gridReturn:list, pos:Vector2,direction,typeCase):
    timeProgress = 0
    if typeCase == "L":
        timeProgress = 25
    else:
        timeProgress = InfoCases[typeCase].density
    # if typeCase == CaseType.grass:
    #     timeProgress = 2
    # elif typeCase == CaseType.water:
    #     timeProgress = 7
    # elif typeCase == CaseType.tree:
    #     timeProgress = 3
    gridReturn[pos.y][pos.x] = direction
    # gridOfLife[pos.y][pos.x] = timeProgress
    listCaseToUpdate[timeProgress].append(pos)


def pathFinding_grid_get(game:GameLevel,endPos:Vector2):
    gameSize = game.size_get()

    gridReturn = [[None for _ in range(gameSize.x)] for _ in range(gameSize.y)]
    # gridOfLife = [[     -1      for _ in range(gameSize.x)] for _ in range(gameSize.y)]

    listCaseToUpdate = [list() for i in range(50)]
    elemtenRestant = 1
    pathFinding_grid_addItemToList(listCaseToUpdate,gridReturn,endPos,Vector2(0,0),1)
    
    castleLevel = game.case_get(endPos).level


    # neighborCase =(
    #     Vector2(-1,-1),Vector2(0,-1),Vector2(1,-1),
    #     Vector2(-1, 0)              ,Vector2(1, 0),
    #     Vector2(-1, 1),Vector2(0, 1),Vector2(1, 1))
    
    #              Principaux     Secondaire
    neighborCase =(Vector2(0,-1) ,Vector2(-1,-1),
                   Vector2(0, 1) ,Vector2( 1,-1),
                   Vector2(-1,0) ,Vector2(-1, 1),
                   Vector2(1, 0) ,Vector2( 1, 1) )
                
    
    count = 0
    while elemtenRestant > 0 and count<= 1500:
        # print("PathFinding Restant :",elemtenRestant)
        # listCaseToUpdate_local = listCaseToUpdate[:]
        listCaseToUpdate_local = listCaseToUpdate[0]
        

        count +=1 
        for iCase in listCaseToUpdate_local:
            # timeToLife = gridOfLife[iCase.y][iCase.x]
            # if timeToLife == 0:
            caseParent = game.case_get(iCase)
            for i_direction_principaux in range(4):
                for i_direction_secondaires in range(2):
                    iDirection = neighborCase[i_direction_principaux*2+i_direction_secondaires]
                    caseNeighbor_pos:Vector2 = iCase+iDirection

                    # get case if exist 
                    caseNeighbor_Data:Case = game.case_get(caseNeighbor_pos)
                    # Test if case 
                    if caseNeighbor_Data:
                        # Test if case was not always calculate 
                        if gridReturn[iCase.y+iDirection.y][iCase.x+iDirection.x] is None :
                            caseNeighbor_Level = caseNeighbor_Data.level + InfoCases[caseNeighbor_Data.type].level
                            caseParent_Level = caseParent.level + InfoCases[caseParent.type].level

                            if caseParent_Level == caseNeighbor_Level: # == castleLevel
                                elemtenRestant += 1
                                pathFinding_grid_addItemToList(listCaseToUpdate,gridReturn,caseNeighbor_pos,iDirection*-1,caseParent.type)
                            else:
                                elemtenRestant += 1
                                pathFinding_grid_addItemToList(listCaseToUpdate,gridReturn,caseNeighbor_pos,iDirection*-1,"L")
                        else:
                            break
                    else:
                        break
            elemtenRestant -= 1
        listCaseToUpdate.append(list())
        listCaseToUpdate.pop(0)
            # elif timeToLife > 0:
            #     gridOfLife[iCase.y][iCase.x] -= 1
            #     listCaseToUpdate.append(iCase)
    if count >300:
        print("Pathfinding Stop by security !")
    return gridReturn






def loadNewTerrain(MainLevel:GameLevel):
    import NoiseLib
    hightMap = NoiseLib.NoiseMap(MainLevel.size_get()//2)
    hightMap.new()
    hightMap.floorToLevel(4)
    # hightMap.test_color()
    print("New Level :")
    hightMap.test_print_color()
    MainLevel.load_heightMap_simple(hightMap.noise,Vector2(2,2))