

from random import randint, seed
from CaseType import CaseType  


# 0 = Null
# o = Oui 
# n = Non

f  = 256 # False = not equal
t  = 255 # True = equal
n  = 254 # Null = on s'en fout
d  = 253 # down
u  = 252 # up
d2 = 251 # down or equal
u2 = 250 # up or equal

type_One = 1
type_Random = 2


neighborCase =(
    (0,0),(1,0),(2,0),
    (0,1)      ,(2,1),
    (0,2),(1,2),(2,2))


def random_resetSeed():
    seed(10)


class RuleM3:
    def __init__(self,mType,mLevel,tile,tilesType=type_One) -> None:
        self.m = mType
        self.l = mLevel
        self.t = tile
        self.tt = tilesType

    def test_level(self,caseData,situation):
        l = self.l
        heigh = caseData.level

        for ix,iy in neighborCase:
            vLevel = l[iy][ix]
            caseSituation = situation[iy][ix]
            if caseSituation == None:
                caseSituation = caseData
            sLevel = caseSituation.level -heigh
            if vLevel != n:
                if vLevel == 0:
                    if 0 != sLevel:
                        return False
                elif vLevel == d:
                    if 0 <= sLevel:
                        return False
                elif vLevel == u:
                    if 0 >= sLevel:
                        return False
                elif vLevel == d2:
                    if 0 < sLevel:
                        return False
                elif vLevel == u2:
                    if 0 > sLevel:
                        return False
                elif vLevel != sLevel:
                    return False
        return True
                    
    def test_type(self,caseData,situation):
        m = self.m
        caseType =  caseData.type
        for ix,iy in neighborCase:
            vType = m[iy][ix]
            caseSituation = situation[iy][ix]
            if caseSituation == None:
                caseSituation = caseData
            sType = caseSituation.type
            if vType != n and vType != 0:
                if vType == f:
                    if caseType == sType:
                        return False
                if vType == t:
                    if caseType != sType:
                        return False
                if 0<vType<20 and vType != sType:
                    return False
                if -20<vType<0 and -vType == sType:
                    return False
        return True

    def test(self,situation):
        """situation doit etre une list de list de tuple (heigh,type)"""
        caseData = situation[1][1]
        m = self.m
        l = self.l
        if l != None:
            if self.test_level(caseData,situation):
                if m != None:
                    if self.test_type(caseData,situation):
                        return True
                    else:
                        return False
                else:
                    return True
            else:
                return False
        elif m != None:
            if self.test_type(caseData,situation):
                return True
            else:
                return False
        else:
            return False


    def getTile(self):
        if self.tt == type_Random:
            return self.t[randint(0,len(self.t)-1)]
        else:
            return self.t
                    


class Rule3M_Dynamique():
    def __init__(self) -> None:
        pass
    def getTile(situation):
        situation = [
            [True,True,True],
            [True,False,True],
            [True,True,True]
        ]
        







def newRulesList_zone(template):

    ListRules = [
        RuleM3([
            [f,f,f],
            [f,0,f],
            [f,f,f]
        ],None,
        template.solo,
        type_Random
        ),
        RuleM3([
            [t,t,t],
            [t,0,t],
            [t,t,t]
        ],None,
        template.center,
        ),


        RuleM3([
            [t,t,t],
            [t,0,t],
            [f,t,t]
        ],None,
        template.cornerInvers_TopLeft,
        ),
        RuleM3([
            [t,t,t],
            [t,0,t],
            [t,t,f]
        ],None,
        template.cornerInvers_TopRight,
        ),
        RuleM3([
            [f,t,t],
            [t,0,t],
            [t,t,t]
        ],None,
        template.cornerInvers_BottomLeft,
        ),
        RuleM3([
            [t,t,f],
            [t,0,t],
            [t,t,t]
        ],None,
        template.cornerInvers_BottomRight,
        ),


        RuleM3([
            [n,f,n],
            [t,0,f],
            [n,t,n]
        ],None,
        template.corner_BottomRight,
        ),
        RuleM3([
            [n,f,n],
            [f,0,t],
            [n,t,n]
        ],None,
        template.corner_BottomLeft,
        ),
        RuleM3([
            [n,t,n],
            [t,0,f],
            [n,f,n]
        ],None,
        template.corner_TopRight,
        ),
        RuleM3([
            [n,t,n],
            [f,0,t],
            [n,f,n]
        ],None,
        template.corner_TopLeft,
        ),


        RuleM3([
            [n,n,n],
            [t,0,f],
            [n,n,n]
        ],None,
        template.right,
        ),
        RuleM3([
            [n,n,n],
            [f,0,t],
            [n,n,n]
        ],None,
        template.left,
        ),
        RuleM3([
            [n,t,n],
            [n,0,n],
            [n,f,n]
        ],None,
        template.top,
        ),
        RuleM3([
            [n,f,n],
            [n,0,n],
            [n,t,n]
        ],None,
        template.bottom,
        ),


        RuleM3([
            [f,f,f],
            [f,0,f],
            [f,t,f]
        ],None,
        template.bottom
        ),
        RuleM3([
            [f,t,f],
            [f,0,f],
            [f,f,f]
        ],None,
        template.top
        ),
        RuleM3([
            [f,f,f],
            [t,0,f],
            [f,f,f]
        ],None,
        template.right
        ),
        RuleM3([
            [f,f,f],
            [f,0,t],
            [f,f,f]
        ],None,
        template.left
        )
    ]
    return ListRules



def newRulesList_path(template):
    
    ListRules = [
        RuleM3([
            [f,f,f],
            [f,0,f],
            [f,f,f]
        ],None,
        template.solo,
        ),
        RuleM3([
            [n,t,n],
            [t,0,t],
            [n,t,n]
        ],None,
        template.center,
        ),


        RuleM3([
            [n,f,n],
            [t,0,t],
            [n,f,n]
        ],None,
        template.x,
        ),
        RuleM3([
            [n,t,n],
            [f,0,f],
            [n,t,n]
        ],None,
        template.y,
        ),

        
        RuleM3([
            [n,f,n],
            [t,0,f],
            [n,f,n]
        ],None,
        template.left,
        ),
        RuleM3([
            [n,f,n],
            [f,0,t],
            [n,f,n]
        ],None,
        template.right,
        ),
        RuleM3([
            [n,f,n],
            [f,0,f],
            [n,t,n]
        ],None,
        template.top,
        ),
        RuleM3([
            [n,t,n],
            [f,0,f],
            [n,f,n]
        ],None,
        template.bottom,
        ),


        RuleM3([
            [n,f,n],
            [t,0,f],
            [n,t,n]
        ],None,
        template.corner_BottomRight,
        ),
        RuleM3([
            [n,f,n],
            [f,0,t],
            [n,t,n]
        ],None,
        template.corner_BottomLeft,
        ),
        RuleM3([
            [n,t,n],
            [t,0,f],
            [n,f,n]
        ],None,
        template.corner_TopRight,
        ),
        RuleM3([
            [n,t,n],
            [f,0,t],
            [n,f,n]
        ],None,
        template.corner_TopLeft,
        ),
        

        RuleM3([
            [n,t,n],
            [f,0,t],
            [n,t,n]
        ],None,
        template.croisement_Left,
        ),
        RuleM3([
            [n,t,n],
            [t,0,f],
            [n,t,n]
        ],None,
        template.croisement_Right,
        ),
        RuleM3([
            [n,t,n],
            [t,0,t],
            [n,f,n]
        ],None,
        template.croisement_Top,
        ),
        RuleM3([
            [n,f,n],
            [t,0,t],
            [n,t,n]
        ],None,
        template.croisement_Bottom,
        ),

        
        RuleM3([
            [n,n,n],
            [n,0,n],
            [n,n,n]
        ],None,
        template.solo,
        ),
    ]
    return ListRules


def newRulesList_level(template):

    ListRules = [
        RuleM3(
        None,[
            [0,0,0],
            [0,0,0],
            [0,0,0]],
        template.solo,
        type_Random
        ),


        # RuleM3(
        # None,[
        #     [ n, n, n],
        #     [ n, 0, u],
        #     [ n, u, u]],
        # template.cornerInvers_BottomLeft
        # ),
        # RuleM3(
        # None,[
        #     [ n, n, n],
        #     [ u, 0, n],
        #     [ u, u, n]],
        # template.cornerInvers_BottomRight
        # ),
        # RuleM3(
        # None,[
        #     [ u, u, n],
        #     [ u, 0, n],
        #     [ n, n, n]],
        # template.cornerInvers_TopRight
        # ),
        # RuleM3(
        # None,[
        #     [ n, u, u],
        #     [ n, 0, u],
        #     [ n, n, n]],
        # template.cornerInvers_TopLeft
        # ),

        RuleM3(
        None,[
            [ d, 0, 0],
            [ 0, 0, 0],
            [ 0, 0, 0]],
        template.cornerInvers_BottomLeft
        ),
        RuleM3(
        None,[
            [ 0, 0, d],
            [ 0, 0, 0],
            [ 0, 0, 0]],
        template.cornerInvers_BottomRight
        ),
        RuleM3(
        None,[
            [ 0, 0, 0],
            [ 0, 0, 0],
            [ d, 0, 0]],
        template.cornerInvers_TopLeft
        ),
        RuleM3(
        None,[
            [ 0, 0, 0],
            [ 0, 0, 0],
            [ 0, 0, d]],
        template.cornerInvers_TopRight
        ),



        RuleM3(
        None,[
            [ n, d, n],
            [ d, 0, 0],
            [ n, 0, n]],
        template.corner_BottomLeft
        ),
        RuleM3(
        None,[
            [ n, d, n],
            [ 0, 0, d],
            [ n, 0, n]],
        template.corner_BottomRight
        ),
        RuleM3(
        None,[
            [ n, 0, n],
            [ d, 0, 0],
            [ n, d, n]],
        template.corner_TopLeft
        ),
        RuleM3(
        None,[
            [ n, 0, n],
            [ 0, 0, d],
            [ n, d, n]],
        template.corner_TopRight
        ),



        RuleM3(
        None,[
            [ n,-1, n],
            [ n, 0, n],
            [ n, 0, n]],
        template.bottom
        ),
        RuleM3(
        None,[
            [ n, 0, n],
            [ n, 0, n],
            [ n,-1, n]],
        template.top
        ),
        RuleM3(
        None,[
            [ n, n, n],
            [-1, 0, 0],
            [ n, n, n]],
        template.left
        ),
        RuleM3(
        None,[
            [ n, n, n],
            [ 0, 0,-1],
            [ n, n, n]],
        template.right
        ),

        RuleM3(
        None,[
            [ n, n, n],
            [ n, 0, n],
            [ n, n, n]],
        template.solo,
        type_Random
        ),
    ]
    return ListRules





def newRulesList_level_out(template):

    ListRules = [
        RuleM3(
        None,[
            [0,0,0],
            [0,0,0],
            [0,0,0]],
        template.solo,
        type_Random
        ),


        RuleM3(
        None,[
            [ n, n, n],
            [ n, 0, u],
            [ n, u, n]],
        template.cornerInvers_BottomLeft
        ),
        RuleM3(
        None,[
            [ n, n, n],
            [ u, 0, n],
            [ n, u, n]],
        template.cornerInvers_BottomRight
        ),
        RuleM3(
        None,[
            [ n, u, n],
            [ u, 0, n],
            [ n, n, n]],
        template.cornerInvers_TopRight
        ),
        RuleM3(
        None,[
            [ n, u, n],
            [ n, 0, u],
            [ n, n, n]],
        template.cornerInvers_TopLeft
        ),



        RuleM3(
        None,[
            [ n, n, n],
            [d2, 0, n],
            [ 1,d2, n]],
        template.corner_BottomRight
        ),
        RuleM3(
        None,[
            [ 1,d2, n],
            [d2, 0, n],
            [ n, n, n]],
        template.corner_TopRight
        ),
        RuleM3(
        None,[
            [ n,d2, 1],
            [ n, 0,d2],
            [ n, n, n]],
        template.corner_TopLeft
        ),
        RuleM3(
        None,[
            [ n, n, n],
            [ n, 0,d2],
            [ n,d2, 1]],
        template.corner_BottomLeft
        ),



        RuleM3(
        None,[
            [ n, n, n],
            [ n, 0, n],
            [ n, 1, n]],
        template.bottom
        ),
        RuleM3(
        None,[
            [ n, 1, n],
            [ n, 0, n],
            [ n, n, n]],
        template.top
        ),
        RuleM3(
        None,[
            [ n, n, n],
            [ 1, 0, n],
            [ n, n, n]],
        template.right
        ),
        RuleM3(
        None,[
            [ n, n, n],
            [ n, 0, 1],
            [ n, n, n]],
        template.left
        ),
        RuleM3(
        None,[
            [ n, n, n],
            [ n, 0, n],
            [ n, n, n]],
        template.solo,
        type_Random
        ),
    ]
    return ListRules






def newRulesList_complexe(template):
    ListRules = [
        RuleM3([
            [n,CaseType.wall,n],
            [n,0,CaseType.wall],
            [n,n,n]
        ],[
            [n,u2,n],
            [n,0,n],
            [n,n,n]
        ],
        template[12][6],
        ),
        RuleM3([
            [n,CaseType.wall,n],
            [CaseType.wall,0,n],
            [n,n,n]
        ],[
            [n,u2,n],
            [n,0,n],
            [n,n,n]
        ],
        template[12][4],
        ),
        


        # RuleM3([
        #     [n,n,n],
        #     [n,0,CaseType.wall],
        #     [n,n,n]
        # ],[
        #     [n,u2,n],
        #     [n,0,0],
        #     [n,n,n]
        # ],
        # template[12][3],
        # ),
        # RuleM3([
        #     [n,n,n],
        #     [CaseType.wall,0,n],
        #     [n,n,n]
        # ],[
        #     [n,u2,n],
        #     [0,0,n],
        #     [n,n,n]
        # ],
        # template[12][2],
        # ),



        RuleM3([
            [n,CaseType.wall,n],
            [n,0,n],
            [n,n,n]
        ],[
            [n,u2,n],
            [n,0,n],
            [n,n,n]
        ],
        template[12][5],
        ),


        RuleM3([
            [n,-CaseType.wall,CaseType.wall],
            [n,0,-CaseType.wall],
            [n,n,n]
        ],[
            [n,u2,u2],
            [n,0,u2],
            [n,n,n]
        ],
        template[14][5],
        ),
        RuleM3([
            [CaseType.wall,-CaseType.wall,n],
            [-CaseType.wall,0,n],
            [n,n,n]
        ],[
            [u2,u2,n],
            [u2,0,n],
            [n,n,n]
        ],
        template[14][6],
        ),

        
        RuleM3([
            [n,n,-CaseType.wall],
            [n,0,CaseType.wall],
            [n,n,n]
        ],None,
        template[13][5],
        ),
        RuleM3([
            [-CaseType.wall,n,n],
            [CaseType.wall,0,n],
            [n,n,n]
        ],None,
        template[13][6],
        ),
        

        RuleM3([
            [n,n,n],
            [n,0,CaseType.wall],
            [n,n,n]
        ],None,
        template[13][7],
        ),
        RuleM3([
            [n,n,n],
            [CaseType.wall,0,n],
            [n,n,n]
        ],None,
        template[13][4],
        ),

    ]
    return ListRules




def newRulesList(template):
    template_level = template.template_level

def getGoodTile(rules:list,situation):
    """ Rules : list[RuleM3] """
    for iRule in rules:
        reponce = iRule.test(situation)
        if reponce == True:
            tile = iRule.getTile()
            if isinstance(tile,list):
                return
            return tile

