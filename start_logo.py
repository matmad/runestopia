





image_path = "ressources/logo/logo_x64.png"


import graphics2D
from vector import Vector2
from math import pi, cos, sin, floor

resolution = graphics2D.getResolution()
mainFrame = None
imageFrame = None
textFrame = None

nbr_stats = 0

def animation_start(mainLayer:graphics2D.GuiLayer,_nbr_stats,skipIntro=False):
    global mainFrame,imageFrame, textFrame,nbr_stats

    nbr_stats = _nbr_stats

    mainLayer.surface_fill()
    sizeImage = Vector2(64,64)*3

    backgroundImage = graphics2D.image_load("ressources/templates/background.png")
    mainFrame = graphics2D.Gui_Frame_BackgroundImage(Vector2(0,0),graphics2D.UDim2(1,0,1,0),backgroundImage,(0,0,0,255),(0,0,0,128),4)
    mainLayer.objects_add(mainFrame)

    textFrame = graphics2D.Gui_TextLabel("",graphics2D.UDim2(0.5,0,0.5,0), None, (255,214,132,255), None, 60, None, Vector2(0.5,0.5),None,Vector2(0.5,0.5))
    mainFrame.objects_add(textFrame)

    imageFrame = graphics2D.Gui_Image(image_path,graphics2D.UDim2(0.5,0,0.51,0), sizeImage,None,None,None,Vector2(0.5,0.5))
    mainFrame.objects_add(imageFrame)

    # image_logo = graphics2D.image_load_RGBA(image_path)
    # image_logo_s = vector.Vector2(graphics2D.image_getSize(image_logo))

    # graphics2D.imageLoaded_paste_RGBA(mainLayer,image_logo,)

    if skipIntro == False:

        for i in range(15):
            imageFrame.size_set((sizeImage * sin(i/20*pi)).floor())
            graphics2D.render()
            graphics2D.wait(50)
            
        textTitle = "Runes Topia"

        n_count = len(textTitle)
        for i in range(n_count):
            x = 0.5 +(cos((i/n_count)*pi )- 1)* 0.155  # 0.085
            pos = graphics2D.UDim2(x,0,0.51,0)
            pos.setParent(mainFrame)
            imageFrame.pos_set(pos)
            
            if i < n_count:
                textFrame.text_set(textTitle[:i+1])

            graphics2D.render()
            graphics2D.wait(50)

    imageFrame.size_set(Vector2(64,64)*3)
    graphics2D.render()
    graphics2D.wait(1000)
    graphics2D.oneCycle()


loadAniamtion_nbrFrame = 0

def animation_nextLoad_init():
    global barFrame, barFrame_Text
    
    # --- # Bar load # --- #
    barFrame = graphics2D.Gui_Frame_Drawable(graphics2D.UDim2(0.5,0,0.63,0),graphics2D.UDim2(0.4,0,0,40),None,None,2,Vector2(0.5,0.5))
    mainFrame.objects_add(barFrame)
    barFrame_size = barFrame.size_get().vector2()
    barFrame_Text = graphics2D.Gui_TextLabel("",graphics2D.UDim2(0.5,(barFrame_size.x-barFrame_size.y)//2,0.63,0),None,(250,250,250,255),None,20,None,Vector2(1,0.5),None,Vector2(1,1.5))
    mainFrame.objects_add(barFrame_Text)

    loadText = ["Folder","Assets/","Assets/Entity","Assets/Building","Assets/Elements","Assets/Sprties","Assets/Sprties/Entity","Assets/Sprties/Building","Assets/Sprties/Terrain","Assets/Sprties/Prefab","Assets/Gui","Ready"]
    


def animation_nextLoad(statName:str, _currentCall=0):
    global loadAniamtion_nbrFrame, barFrame, barFrame_Text, nbr_stats

    currentCall = _currentCall + 1
    # print(statName,currentCall)
    
    n_count_A = 30 # Apparition
    n_count_C = 6  # Nbr Frame per Stat Progression 
    n_count_B = n_count_C*(nbr_stats+1) # Default 180 # Progression
    
    loadTextAnimation = ["   ",".  ",".. ","..."]

    color_pu = (206,160,255,255) # purple
    color_pi = (228,98,98,255)   # pink
    fillColor = color_pi

    # --- # Start Animation if not Start # --- #
    if loadAniamtion_nbrFrame == 0:
        loadAniamtion_nbrFrame += 1
        animation_nextLoad_init()
        # for i in range(n_count_A):
        animation_nextLoad(statName,-n_count_A+1)
        
    barFrame_size = barFrame.size_get().vector2()

    loadAniamtion_nbrFrame += 1
    # for i in range(n_count_A+n_count_B):
    i = loadAniamtion_nbrFrame


    delta = int(barFrame_size.y/2)
    thickness = 20
    pA = Vector2(delta,delta).floor()
    pB = Vector2(barFrame_size.x-delta,delta).floor()

    if i < n_count_A:
        stat = (cos((i/n_count_A)*pi )- 1)    
        pA.x = int(barFrame_size.x//2 + (barFrame_size.x//2-delta) *stat*0.5   )
        pB.x = int(barFrame_size.x//2 + (barFrame_size.x//2-delta) *stat*(-0.5))

    barFrame.surface_new()

    
    # Left Point # 
    graphics2D.draw_cercle(barFrame,pA+1,thickness//2,(208,158,73,255))
    # Right Point # 
    graphics2D.draw_cercle(barFrame,pB+1,thickness//2,(208,158,73,255))
    graphics2D.draw_cercle(barFrame,pB+1,(thickness//2)-2,(254,211,133,255))
    # Center Line# 
    graphics2D.draw_line(barFrame,pA,pB,(208,158,73,255),thickness)
    graphics2D.draw_line(barFrame,pA,pB,(254,211,133,255),thickness-4)

    if i >= n_count_A:
        # Progression Line # 
        coefFill = (i-n_count_A)/n_count_B
        fillColor = [color_pi[i]+ (color_pu[i]-color_pi[i])*coefFill for i in range(4)]
        
        # fillColor2 = [int( (236,122,112,255)[i] + ((266,22,100,255)[i]-(236,122,112,255)[i])*coefFill) for i in range(4)]

        # print((pB.x-pA.x)*coefFill+pA.x, delta)
        pC = Vector2((pB.x-pA.x)*coefFill+pA.x , delta).floor()
        graphics2D.draw_line(barFrame,pA,pC,fillColor,thickness-4)
        graphics2D.draw_cercle(barFrame,pC+1,(thickness//2)-2,fillColor)

        pD_progression = ((i-n_count_A)%20)/20
        pD = Vector2((pB.x-pA.x)*pD_progression+pA.x , delta).floor()
        if pD.x < pC.x:
            graphics2D.draw_cercle(barFrame,pD+1,(thickness//2)-2,(236,175,175,255)) # c = (236,122,112,255)
        
        # t = loadText[len(loadText)*(i-n_count_A)//n_count_B] + loadTextAnimation[(i%8)//2]
        t = statName + loadTextAnimation[(i%8)//2]
        barFrame_Text.text_set(t)
        
        if i== n_count_A+n_count_B-4:
            fillColor = (206,160,255,255)

    # Left Point 2 with fill color#
    graphics2D.draw_cercle(barFrame,pA+1,(thickness//2)-2,fillColor)
    
    graphics2D.render()
    graphics2D.wait(50)
    graphics2D.oneCycle()

    # if currentCall >= 0:
    # print(n_count_B,nbr_stats,currentCall,n_count_B//nbr_stats)
    # if n_count_B//nbr_stats > currentCall:
    if n_count_C > currentCall:
        # print(">> Restart Animation")
        if loadAniamtion_nbrFrame < n_count_A+n_count_B:
            animation_nextLoad(statName,currentCall)
        else:
            print("Warn : loadAniamtion_nbrFrame > n_count_A+n_count_B")

    # mainFrame.objects_remove(barFrame)
    # mainFrame.objects_remove(barFrame_Text)

def animation_finish():
    global imageFrame,textFrame, mainFrame
    graphics2D.render()
    graphics2D.wait(500)
    graphics2D.oneCycle()


    # --- # Text And Logo Resize # --- #
    n_count = 12
    for i in range(n_count):
        x =  0.5 + (imageFrame.pos.xScale-0.5)*1.2
        pos = graphics2D.UDim2(x,0,0.5,0)
        pos.setParent(mainFrame)
        imageFrame.pos_set(pos)
        imageFrame.size_set((imageFrame.size_get()*1.2).floor())
        textFrame.frontSize_set(floor(textFrame.frontSize_get()*1.2))

        mainFrame.alphaLevel_set(255-int(255/n_count*i))

        graphics2D.render()
        graphics2D.wait(50)
        graphics2D.oneCycle()

    mainFrame.alphaLevel_set(255)
    mainFrame.fillColor = None
    mainFrame.objects_clear()

    graphics2D.render()
    graphics2D.wait(50)

    mainFrame = None
    imageFrame = None
    textFrame = None






