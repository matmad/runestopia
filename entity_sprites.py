

import graphics2D
from vector import Vector2
import json


sizeSprite = Vector2(32,32)
path_template = "ressources/templates/template_entity.png"
# path_template = "ressources/templates/template_foret.png"



class MainEntityTiles:
    def __init__(self) -> None:
        self.tiles = list()

    def cutTemplate(self,img) -> list():
        imageShape = graphics2D.image_getSize(img)
        sizeImage = Vector2(imageShape[0],imageShape[1])

        tilesList = list()
        for iy in range(sizeImage.y//sizeSprite.y):
            tilesLineList = list()
            for ix in range(sizeImage.x//sizeSprite.x): 
                posPixel = Vector2(ix*sizeSprite.x,iy*sizeSprite.y)
                partImage = graphics2D.image_clip(img,posPixel, sizeSprite)
                tilesLineList.append(partImage )
                
            tilesList.append(tilesLineList)

        return tilesList
    
    def loadTemplate(self,image):
        self.tiles = self.cutTemplate(image)

    def getTiles_range(self,startPos,endPos):
        newList = list()
        for iy in range(startPos.y - endPos.y):
            newList.append(self.tiles[iy][startPos.x:endPos.x]) 
        return newList
    
    def getTiles_list(self,startPos,endPos):
        """ Start pos includ and End pos includ """
        newList = list()
        print("Size : ",endPos.y +1 - startPos.y,endPos.x - startPos.x +1)
        for iy in range(endPos.y  - startPos.y+1):
            for ix in range(endPos.x  - startPos.x+1):
                newList.append(self.tiles[startPos.y+iy][startPos.x+ix]) 
        return newList



mainTemplate = MainEntityTiles()
templateImage = graphics2D.image_load_RGBA(path_template)
mainTemplate.loadTemplate(templateImage)




class EntityTiles:
    def __init__(self,tiles:list, front,back,left,right) -> None:
        self.tiles = tiles

        self.front = front
        self.back = back
        self.left = left
        self.right = right
        
        self.l_front = len(front)
        self.l_back  = len(back)
        self.l_left  = len(left)
        self.l_right = len(right)

    def get_front(self, i):
        return self.tiles[self.front[i%self.l_front]]
    def get_back(self, i):
        return self.tiles[self.back[i%self.l_back]]
    def get_left(self, i):
        return self.tiles[self.left[i%self.l_left]]
    def get_right(self, i):
        return self.tiles[self.right[i%self.l_right]]

    def __getitem__(self, i):
        return self.tiles[i]
    
    def __len__(self):
        return len(self.tiles)




def new_EntityTiles(startPos,endPos,tilesIdList):
    return EntityTiles(mainTemplate.getTiles_list(startPos,endPos),tilesIdList[0],tilesIdList[1],tilesIdList[2],tilesIdList[3])




if __name__ == "__main__":
    graphics2D.init(Vector2(128,128),"Test - Entity Sprites Animation")


    entityList = list()

# --- # Open with Python # --- # 
    """
    # list All Entity .add New Entity           ||  Pos in Template : Start Pos ,  End Pos ||Animation : Front    Back      Left   Right
    entityList.append(Entity(EntityTiles(mainTemplate.getTiles_list(Vector2(1,0),Vector2(3,3)) ,       [6,7,8],[9,10,11],[0,1,2],[3,4,5])))
    entityList.append(Entity(EntityTiles(mainTemplate.getTiles_list(Vector2(4,0),Vector2(6,3)) ,       [6,7,8],[9,10,11],[0,1,2],[3,4,5])))
    # Can add the next sprites, here (copy, past, the line before ^ )
    """

# --- # Open with Json File # --- # 
    # Opening JSON file
    f = open('ressources\entity.json')
    
    # returns JSON object as 
    # a dictionary
    data = json.load(f)
    
    # Iterating through the json
    # list
    for i in data:
        print(" > ",i['name'])
        entityList.append(Entity(EntityTiles(mainTemplate.getTiles_list(Vector2(i["tiles"]["start"][0],i["tiles"]["start"][1]),Vector2(i["tiles"]["end"][0],i["tiles"]["end"][1])) , i["animation"]["front"],i["animation"]["back"],i["animation"]["left"],i["animation"]["right"])))
    
    # Closing file
    f.close()

# --- # Open with Yaml File # --- # 
    """
    import yaml

    with open("example.yaml", "r") as stream:
        try:
            print(yaml.safe_load(stream))
        except yaml.YAMLError as exc:
            print(exc)
    """

# --- # Next Porgramme # --- # 


    # Setup Layer for display sprites 
    mainLayer = graphics2D.layer_new()

    # Preview of template
    graphics2D.imageLoaded_paste(mainLayer,templateImage,Vector2(0,0))
    graphics2D.render()
    graphics2D.wait(1000)

    entitySelect = 1  # Select your Entity 
    pos = Vector2(16,16) # position of annimation 

    # Sprites Test 
    spriteTest_p = Vector2(0,0) # Position 
    spriteTest_v = Vector2(0,0) # Velocity
    spriteTest_s = 0.5 # Speed

    # Loop to display sprites 
    iFrame = 0
    while not(graphics2D.oneCycle()): # Test if the window was don't close 
        iFrame += 1

        # Get Sprite
        entity:Entity = entityList[entitySelect]

        # Reset Display
        mainLayer.surface_fill((0,0,0,255))

        # Draw sprites 
        graphics2D.imageLoaded_paste(mainLayer,entity.tiles.get_front(iFrame),pos+sizeSprite.OnlyX())
        graphics2D.imageLoaded_paste(mainLayer,entity.tiles.get_back(iFrame), pos+sizeSprite.OnlyX()+sizeSprite.OnlyY()*2)
        graphics2D.imageLoaded_paste(mainLayer,entity.tiles.get_left(iFrame), pos+sizeSprite.OnlyY())
        graphics2D.imageLoaded_paste(mainLayer,entity.tiles.get_right(iFrame),pos+sizeSprite.OnlyY()+sizeSprite.OnlyX()*2)

        spriteTest_p += spriteTest_v
        spriteTest_v = Vector2(spriteTest_s,0)  if spriteTest_p.x <= 0.5 and spriteTest_p.y <= 2.5 else spriteTest_v
        spriteTest_v = Vector2(0,spriteTest_s)  if spriteTest_p.x >= 0.5 and spriteTest_p.y >= 2.5 else spriteTest_v
        spriteTest_v = Vector2(-spriteTest_s,0) if spriteTest_p.x >= 2.5 and spriteTest_p.y >= 0.5 else spriteTest_v
        spriteTest_v = Vector2(0,-spriteTest_s) if spriteTest_p.x <= 2.5 and spriteTest_p.y <= 0.5 else spriteTest_v
        graphics2D.imageLoaded_paste(mainLayer,entity.getTile(spriteTest_v,iFrame),spriteTest_p*sizeSprite)


        # Update Screen
        graphics2D.render()

        # Wait 
        graphics2D.wait(300)


