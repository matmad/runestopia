

# Une des première "librairi" que j'ai fais, sa vole pas haut mais marche #


from perlin_noise import PerlinNoise


ConsolColors = {
    "HEADER"    : '\033[95m',
    "OKBLUE"    : '\033[94m',
    "OKCYAN"    : '\033[96m',
    "OKGREEN"   : '\033[92m',
    "WARNING"   : '\033[93m',
    "FAIL"      : '\033[91m',
    "ENDC"      : '\033[0m' ,
    "BOLD"      : '\033[1m' ,
    "UNDERLINE" : '\033[4m' }


class NoiseMap():
    def __init__(self,size) -> None:
        self.s = size[0],size[1]
        self.h = 1
        self.nbr_level = 1

        self.noise = list()

    def new(self):
        noise1 = PerlinNoise(octaves=3)
        noise2 = PerlinNoise(octaves=6)
        noise3 = PerlinNoise(octaves=12)
        noise4 = PerlinNoise(octaves=24)

        self.noise = []
        sx = self.s[0]
        sy = self.s[1]

        for iy in range(sy):
            line = []
            for ix in range(sx):
                noise_val  = noise1([ix/sx, iy/sy])
                noise_val += noise2([ix/sx, iy/sy]) * 0.5 
                noise_val += noise3([ix/sx, iy/sy]) * 0.25 
                noise_val += noise4([ix/sx, iy/sy]) * 0.125 
                line.append(noise_val)
            self.noise.append(line)
        return self.noise
    
    def floorToLevel(self,nbrLevel):
        plageLevel = 1 / nbrLevel
        plageLevelPer2 = plageLevel/2

        for iLine in self.noise:
            for ix in range(len(iLine)):
                iLine[ix] = int((abs(iLine[ix]+0.5)) // plageLevel)
        return self.noise

    def test_print(self):
        for iLine in self.noise:
            s = ""
            for ix in iLine:
                v = str(ix)
                s += v + " "*(3-len(v))
            print(s)
        return
    
    def test_color(self):
        colors = ['\033[95m''\033[94m','\033[96m','\033[92m','\033[93m','\033[91m']
        ['\033[0m ','\033[1m ','\033[4m ',]
        s = '\033[1m ' # met en grass
        for i, c in enumerate(colors):
            s += c+str(i)
        s += '\033[0m ' # remet en normale 
        print(s)

    def test_print_color(self):
        colors = ['\033[95m''\033[94m','\033[96m','\033[92m','\033[93m','\033[91m']
        for iLine in self.noise:
            s = '\033[1m '  # met en grass
            for ix in iLine:
                v = str(ix)
                s += colors[ix] if ix < 5 else ""
                s += v + " "*(3-len(v))
            s += '\033[0m '  # remet en normale 
            print(s)
        return
    
    def test_print_color_mini(self):
        colors = ['\033[95m''\033[94m','\033[96m','\033[92m','\033[93m','\033[91m']
        for iLine in self.noise:
            s = '\033[1m'  # met en grass
            for ix in iLine:
                v = str(ix)
                s += colors[ix] if ix < 5 else ""
                s += v
            s += '\033[0m'  # remet en normale 
            print(s)
        return




