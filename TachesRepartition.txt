**Repartition des Taches**
__Mathis__
```diff
+ Bug Start Animation : color
- Caluclue du time / day / nbr Waves 
- Pathfinding (Updrades)
- Many Pathfindings (Allier / enemy)
- Cycle day/night
- Entity Deplacements 
- Entity Attack 
- Bug : Fixe castle (no entity -> terrain)
- Place Castle at Start of game
+ Counter of Ressources                                                  + 
+ Correction of "Order Layout List"                                      | 1h
+ Bug : Correction Proportionnality in Start Animation                   +
- Data Funtions (remove ressources, getTime, getEntity, getBuildings)
- upgrad Place Buildings (add link in data)
- Bug : error in zoom counter Ressources (Frame)
- Bug : Place Buildings only spot possible
+ Add system of skip intro                                               +
+ Bug : Camera not dezoom Max                                            |
+ Bug : Error of Resize Image in Scrollable Frame (Priority)             |
+ Bug : Error Scrollable Frame function visibleObject                    |
+ Bug : Error Scrollable Frame possition of object                       |
+ Bug : Error Scrollable Frame possition of camera                       | Corriged in 2h 
+ Bug : Display : Camera Move                                            |
+ Bug : Display : Camera Zoom at position sizeCursor                     |
+ Bug : Display : function translate_pixelPos_to_tilePos                 |
+ Bug : Display : Origine entity frame not alligne with terrain frame    +
+ Bug : when terrains's size no proportionnal to noise's size        +
+ Bug : use two Noise Map (One for terrain and seconde for tree)     | 1h 
+ Upgrade Systeme of terrain generation (most Beauty)                +
+ Bug Start Animation : left point color false           (1 min)     
+ Pathfinding : update system to effeciency               (30 min) (result : time /2)
+ Pathfinding : Add systeme to entity change Level
+ Bug : Pathfinding : Entity priority of level of castle not the fast path of the actual level of entity
+ Bug : Correction Border of Terrain 
```
__Andreas__
```diff
- Drawn Cards
- Move Cards (ou Mathis) 
- Place Cards (Buildings / Entity)
+ Continu Json (Entity / Buildings) 
- Less Wooden Wall (Json Buildings)
```
__Mikael__
```diff
- Spawns of enemy / Gestion of difficulty per waves
- Top Bar Info (Show Time, nbr Waves, Health, Enemy nb, ...)
+ Continu Graphics Sprites 
```