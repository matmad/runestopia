
from math import floor

ConsolColors = {
    "HEADER" : '\033[95m',
    "OKBLUE" : '\033[94m',
    "OKCYAN" : '\033[96m',
    "OKGREEN" : '\033[92m',
    "WARNING" : '\033[93m',
    "FAIL" : '\033[91m',
    "ENDC" : '\033[0m',
    "BOLD" : '\033[1m',
    "UNDERLINE" : '\033[4m'}


class Vector2:
    def __init__(self, x=None,y=None) -> None:
        self.x = x
        self.y = y
        if y != None and (isinstance(y,float)or isinstance(y,int)):
            self.x = x
            self.y = y
        elif isinstance(x,Vector2) or isinstance(x,Vector3):
            self.x = x.x
            self.y = x.y
        elif x != None and (isinstance(x,list) or isinstance(x,tuple)):
            if len(x) >= 2:
                self.x = x[0]
                self.y = x[1]
        # elif isinstance(x,None) and isinstance(y,None):
            # self.x = 0
            # self.y = 0


    def __repr__(self):  
        """Afiche in console"""
        return "Vector2({},{})".format(self.x, self.y)

    def __str__(self):  
        """ When you want do a print"""
        return "Vector2({},{})".format(self.x, self.y)


# --------------------------------------- #


    def __getitem__(self, i):
        return (self.x, self.y)[i]

    def __setitem__(self, i, v):
        if i ==0:
            self.x = v
        elif i ==1:
            self.y = v

    def __len__(self):
        return 2


# --------------------------------------- #



    def __add__(self, other):
        """ Addition Operation (-)"""
        if isinstance(other, Vector2):
            return Vector2(self.x + other.x, self.y+ other.y)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector2(self.x+ other, self.y+ other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 2:
                return Vector2(self.x+ other[0], self.y+ other[1])
        print(ConsolColors["HEADER"] +"Error Vector operation (+) null"+ConsolColors["ENDC"] )


    def __sub__(self, other):
        """ Remove Operation (-)"""
        if isinstance(other, Vector2):
            return Vector2(self.x - other.x, self.y- other.y)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector2(self.x- other, self.y- other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 2:
                return Vector2(self.x- other[0], self.y- other[1])
        print(ConsolColors["HEADER"] +"Error Vector operation (-) null"+ConsolColors["ENDC"] )


    def __mul__(self, other):
        """ Multi Operation (*)"""
        if isinstance(other, Vector2):
            return Vector2(self.x * other.x, self.y * other.y)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector2(self.x* other, self.y* other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 2:
                return Vector2(self.x* other[0], self.y* other[1])
        print(ConsolColors["HEADER"] +"Error Vector operation (*) null"+ConsolColors["ENDC"] )
        

    def __truediv__(self, other):
        """ Basic Divition Operation (/)"""
        if isinstance(other, Vector2):
            return Vector2(self.x / other.x, self.y / other.y)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector2(self.x/ other, self.y/ other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 2:
                return Vector2(self.x/ other[0], self.y/ other[1])
        print(ConsolColors["HEADER"] +"Error Vector operation (/) null"+ConsolColors["ENDC"] )


    def __floordiv__(self, other):
        """ Floor Divition Operation (//)"""
        if isinstance(other, Vector2):
            return Vector2(self.x // other.x, self.y // other.y)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector2(self.x// other, self.y// other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 2:
                return Vector2(self.x// other[0], self.y// other[1])
        print(ConsolColors["HEADER"] +"Error Vector operation (//) null"+ConsolColors["ENDC"] )

        
    def __mod__(self, other):
        """ Modulo Operation (%)"""
        if isinstance(other, Vector2):
            return Vector2(self.x % other.x, self.y % other.y)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector2(self.x% other, self.y% other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 2:
                return Vector2(self.x% other[0], self.y% other[1])
        print(ConsolColors["HEADER"] +"Error Vector operation (%) null"+ConsolColors["ENDC"] )
        

    def __pow__(self, other):
        """ Power Operation (%)"""
        if isinstance(other, Vector2):
            return Vector2(self.x ** other.x, self.y ** other.y)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector2(self.x** other, self.y** other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 2:
                return Vector2(self.x** other[0], self.y** other[1])
        print(ConsolColors["HEADER"] +"Error Vector operation (**) null"+ConsolColors["ENDC"] )


# ------------------------------------------------ #


    def __eq__(self, other: object) -> bool:
        """ Condition Operation (==)"""
        if isinstance(other, Vector2):
            return self.x == other.x and self.y == other.y
        elif isinstance(other, int) or isinstance(other, float):
            return self.x == other and self.y == other
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 2:
                return self.x == other[0] and self.y == other[1]
        print(ConsolColors["HEADER"] +"Error Vector conditional operation (==) null"+ConsolColors["ENDC"] )
        

    def __ne__(self, other: object) -> bool:
        """ Condition Operation (!=)"""
        if isinstance(other, Vector2):
            return self.x != other.x and self.y != other.y
        elif isinstance(other, int) or isinstance(other, float):
            return self.x != other and self.y != other
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 2:
                return self.x != other[0] and self.y != other[1]
        print(ConsolColors["HEADER"] +"Error Vector conditional operation (!=) null"+ConsolColors["ENDC"] )


# ------------------------------------------------ #

    """
    &	__and__(self, other)
    |	__or__(self, other)
    ^	__xor__(self, other)
    """

# ------------------------------------------------ #

    def OnlyX(self):
        return Vector2(self.x, 0)

    def OnlyY(self):
        return Vector2(0, self.y)

    def DistanceAuCarre(self):
        return self.x**2 + self.y**2

    def Distance(self):
        return self.DistanceAuCarre() ** 0.5

    def d(self):
        return self.Distance()
        
    def floor(self):
        return Vector2(floor(self.x),floor(self.y))
    
    def vector2(self):
        return self
    
    def copy(self):
        return Vector2(self.x,self.y)

# ---------------------------------- #

"""
+	__add__(self, other)
–	__sub__(self, other)
*	__mul__(self, other)
/	__truediv__(self, other)
//	__floordiv__(self, other)
%	__mod__(self, other)
**	__pow__(self, other)
>>	__rshift__(self, other)
<<	__lshift__(self, other)
&	__and__(self, other)
|	__or__(self, other)
^	__xor__(self, other)
"""





class Vector3:
    def __init__(self, *val) -> None:
        ##self.value = val
        self.x = val[0]
        self.y = val[1]
        self.z = val[2]


# --------------------------------------- #


    def __getitem__(self, i):
        return (self.x, self.y, self.z)[i]


# --------------------------------------- #


    def __add__(self, other):
        """ Addition Operation (-)"""
        if isinstance(other, Vector3):
            return Vector3(self.x + other.x, self.y+ other.y, self.z+ other.z)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector3(self.x+ other, self.y+ other, self.z+ other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 3:
                return Vector3(self.x+ other[0], self.y+ other[1], self.z+ other[2])
        print(ConsolColors["HEADER"] +"Error Vector operation (+) null"+ConsolColors["ENDC"] )


    def __sub__(self, other):
        """ Remove Operation (-)"""
        if isinstance(other, Vector3):
            return Vector3(self.x - other.x, self.y- other.y, self.z- other.z)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector3(self.x- other, self.y- other, self.z- other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 3:
                return Vector3(self.x- other[0], self.y- other[1], self.z- other[2])
        print(ConsolColors["HEADER"] +"Error Vector operation (-) null"+ConsolColors["ENDC"] )


    def __mul__(self, other):
        """ Multi Operation (*)"""
        if isinstance(other, Vector3):
            return Vector3(self.x * other.x, self.y * other.y, self.z* other.z)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector3(self.x* other, self.y* other, self.z* other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 3:
                return Vector3(self.x* other[0], self.y* other[1], self.z* other[2])
        print(ConsolColors["HEADER"] +"Error Vector operation (*) null"+ConsolColors["ENDC"] )
        

    def __truediv__(self, other):
        """ Basic Divition Operation (/)"""
        if isinstance(other, Vector3):
            return Vector3(self.x / other.x, self.y / other.y, self.z/ other.z)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector3(self.x/ other, self.y/ other, self.z/ other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 3:
                return Vector3(self.x/ other[0], self.y/ other[1], self.z/ other[2])
        print(ConsolColors["HEADER"] +"Error Vector operation (/) null"+ConsolColors["ENDC"] )


    def __floordiv__(self, other):
        """ Floor Divition Operation (//)"""
        if isinstance(other, Vector3):
            return Vector3(self.x // other.x, self.y // other.y, self.z// other.z)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector3(self.x// other, self.y// other, self.z// other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 3:
                return Vector3(self.x// other[0], self.y// other[1], self.z// other[2])
        print(ConsolColors["HEADER"] +"Error Vector operation (//) null"+ConsolColors["ENDC"] )

        
    def __mod__(self, other):
        """ Modulo Operation (%)"""
        if isinstance(other, Vector3):
            return Vector3(self.x % other.x, self.y % other.y, self.z% other.z)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector3(self.x% other, self.y% other, self.z% other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 3:
                return Vector3(self.x% other[0], self.y% other[1], self.z% other[2])
        print(ConsolColors["HEADER"] +"Error Vector operation (%) null"+ConsolColors["ENDC"] )
        

    def __pow__(self, other):
        """ Power Operation (%)"""
        if isinstance(other, Vector3):
            return Vector3(self.x ** other.x, self.y ** other.y, self.z** other.z)
        elif isinstance(other, int) or isinstance(other, float):
            return Vector3(self.x** other, self.y** other, self.z** other)
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 3:
                return Vector3(self.x** other[0], self.y** other[1], self.z** other[2])
        print(ConsolColors["HEADER"] +"Error Vector operation (**) null"+ConsolColors["ENDC"] )


# ------------------------------------------------ #


    def __eq__(self, other: object) -> bool:
        """ Condition Operation (==)"""
        if isinstance(other, Vector3):
            return self.x == other.x and self.y == other.y and self.z == other.z
        elif isinstance(other, int) or isinstance(other, float):
            return self.x == other and self.y == other and self.z == other
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 3:
                return self.x == other[0] and self.y == other[1] and self.z == other[2]
        print(ConsolColors["HEADER"] +"Error Vector conditional operation (==) null"+ConsolColors["ENDC"] )
        

    def __ne__(self, other: object) -> bool:
        """ Condition Operation (!=)"""
        if isinstance(other, Vector3):
            return self.x != other.x and self.y != other.y and self.z != other.z
        elif isinstance(other, int) or isinstance(other, float):
            return self.x != other and self.y != other and self.z != other
        elif isinstance(other, list) or isinstance(other, tuple):
            if len(other) >= 3:
                return self.x != other[0] and self.y != other[1] and self.z != other[2]
        print(ConsolColors["HEADER"] +"Error Vector conditional operation (!=) null"+ConsolColors["ENDC"] )



# ------------------------------------------------ #

    def OnlyX(self):
        return Vector3(self.x, 0)

    def OnlyY(self):
        return Vector3(0, self.y)
        
    def OnlyZ(self):
        return Vector3(0, self.z)

    def DistanceAuCarre(self):
        return self.x**2 + self.y**2 + self.z**2

    def Distance(self):
        return self.DistanceAuCarre() ** 0.5

    def d(self):
        return self.Distance()


    
    def ToScreen():
        pass 
        



# ---------------------------------- #
